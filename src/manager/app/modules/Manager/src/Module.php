<?php

namespace Manager;

use Intelletto\Webservice\Service\Authentication;
use Intelletto\Webservice\Service\Acl;
use Intelletto\Webservice\Service\Mail;
use Intelletto\Webservice\Service\Form;
use Intelletto\Webservice\Service\Data;
use Intelletto\Webservice\Service\Role;
use Intelletto\Webservice\Service\User;

class Module extends \Intelletto\Bootstrap\Module {

    public function registerServices($di) {
        parent::registerServices($di);
        
        $di->set('authentication', function() use ($di) {
            return new Authentication($di->get('webservice'));
        });
        
        $di->set('aclService', function() use($di) {
            return new Acl($di->get('webservice'));
        });
        
        $di->set('form', function() use($di) {
            return new Form($di->get('webservice'));
        });
        
        $di->set('dataService', function() use($di) {
            return new Data($di->get('webservice'));
        });
        
        $di->set('mailService', function() use($di) {
            return new Mail($di->get('webservice'));
        });

        $di->set('user', function() use($di) {
            return new User($di->get('webservice'));
        });

        $di->set('role', function() use($di) {
            return new Role($di->get('webservice'));
        });
        
        $di->get('flashSession')->setCssClasses([
            'success' => 'ui small green message', 
            'error' => 'ui small red message'
        ]);

        $userId = $di['session']->identity['userId'];

        $di['view']->listSystems = $di->get('user')->systems($userId)->getData();

        $lastAccess = $di->get('user')->lastAccess($userId)->getData()['created'];
        $di['view']->lastAccess = date('d/m/Y H:i:s', strtotime($lastAccess));

        $roles = $di->get('role')->byUserAndSystem($userId, 3)->getData();
        $di['view']->listRoles = $roles;


        $di['view']->userName = $di['session']->identity['userName'];
        $di['view']->roleId = $di['session']->identity['role_id'];

        $di['view']->redirect_system = $di['config']->idp->url;
        $di['view']->appname = $di['config']->view->appname;

        setcookie('http_x_forwarded_host', $di['config']->sisrel->http_x_forwarded_host, time() + 14400,'/');
        setcookie('perfil_cookie', base64_encode($di['config']->sisrel->perfil_cookie), time() + 14400, '/');
        setcookie('co_usuario', $di['view']->userId, time() + 14400, '/');
    }

}