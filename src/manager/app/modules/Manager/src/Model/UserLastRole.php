<?php

namespace Manager\Model;

use Phalcon\Mvc\Model;
use Phalcon\Mvc\Model\Resultset\Simple as Resultset;

class UserLastRole extends Model
{
    public $id;
    public $role_id;
    public $user_id;
    public $system_id;
    public $created;

    public function initialize()
    {
        $this->setSchema("public");
        $this->setSource("user_last_role");
    }
}