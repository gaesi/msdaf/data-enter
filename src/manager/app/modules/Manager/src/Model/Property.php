<?php

namespace Manager\Model;

use Phalcon\Mvc\Model;
use Phalcon\Mvc\Model\Resultset\Simple as Resultset;

class Property extends Model 
{
    public $id;
    public $key;
    public $value;

    public function initialize() 
    {
        $this->setSchema("public");
        $this->setSource("property");
    }
}