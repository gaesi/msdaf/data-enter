<?php

namespace Manager\Service;

use GuzzleHttp\Client as Guzzle;

class Sisrel
{
    protected function getClient()
    {
        return new Guzzle();
    }
    
    protected function request($url, $params)
    {
        $url = 'http://' . $_SERVER['HTTP_HOST'] . $url;
        $cookie = $_COOKIE;                 

        unset($cookie['PHPSESSID']);
        
        return $this->getClient()->get($url, [
            'query' => $params, 
            'cookies' => $cookie
        ]);
    }
    
    public function getReports($url, array $params = [])
    {
        $params['action'] = 'list-report';
        
        return $this->request($url, $params)->json();
    }
    
    public function getColValues($url, array $params)
    {
        $params['action'] = 'get-col-values';
            
        return $this->request($url, $params)->json();
    }
}