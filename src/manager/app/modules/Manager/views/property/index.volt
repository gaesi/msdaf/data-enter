<style type="text/css" media="screen">
    .ui.form .fields .field:first-child{padding-left:1%;}
</style>
<div class="thirteen wide column">
    <h3 class="body-title-page" data-menu=".propriedades" style="margin-bottom: 30px;">
        Propriedades
    </h3>
    {{ flashSession.output() }}
    <div class="ui">
        <form class="ui form" action="manager/property/save" method="post" accept-charset="utf-8">
            <h3 class="header">Globais</h3>
            <div class="ui segment">
                <div class="three fields global"></div>
            </div>
            
            <h3 class="header">Formulários</h3>
            
            <div class="ui fluid accordion">
                {% set globalFields = [] %}
                {% for row in data %}
                    {% set struct = row['struct']|json_decode %}
                    <div class="title">
                        <i class="dropdown icon"></i>
                        {{ row['name'] }}
                    </div>
                    <div class="content three fields">
                        {% for field in struct if field.type == 'options' %}
                            {% set key = field.adapter == 'form' ? field.field : field.reference %}
                            {% if !key %}
                                {% set key = field.nativeName %}
                            {% endif %}
                            
                            {% if key not in globalFields %}
                                {% set value = row['id'] ~ '.' ~ field.nativeName %}

                                <div class="field {{ field.adapter }}">
                                    <div class="ui toggle checkbox">
                                        {% if value in fields %}
                                            {{ check_field('field[]', 'value': value, 'checked': 'checked') }}
                                        {% else %}
                                            {{ check_field('field[]', 'value': value) }}
                                        {% endif %}
                                        <label>{{ field.name }}</label>
                                    </div>
                                </div>
                            {% endif %}
                            <?php $globalFields[] = $key ?>
                        {% endfor %}
                    </div>
                {% elsefor %}
                    <div class="ui small warning center aligned message margin-default">
                        Nenhum formulário foi cadastrado.
                    </div>
    	        {% endfor %}
                
                {% if data|length %}
                    <div class="ui segment basic floated right" style="margin:0;padding: 1em 0 0 0">
                        <button type="submit" class="ui icon button"><i class="save icon"></i> Salvar</button>
                    </div>
                {% endif %}
            </div>
        </form>
    </div>
</div>

<script type="text/javascript">
    $('.fields.global').append($('.field.sisrel'));
    $('.fields.global').append($('.field.form'));
    
    $('.accordion .content').each(function(){
        if ($(this).find('.field').length == 0) {
            $(this).prev().remove();
            $(this).remove();
        }
    });
    
    $('.propriedades').addClass('active');
    $('.checkbox').checkbox();
</script>