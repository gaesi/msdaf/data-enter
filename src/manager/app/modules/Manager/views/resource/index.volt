<div class="thirteen wide column">
    <h2 class="ui header" data-menu=".recursos">Recursos</h2>
    {{ flashSession.output() }}
    <div class="ui">
        <a href="manager/resource/form" class="ui right floated blue submit button">Novo</a>
        <form action="manager/resource" method="get">
            <div class="ui action input">
                <input name="term" placeholder="Procurar..." type="text" value="{{ term }}">
                <div class="ui icon button"><i class="search icon"></i></div>
            </div>
        </form> 
        <table class="ui sortable table segment">
            <thead>
                <tr>
                    <th>Descrição</th>
                    <th></th>
                </tr>
            </thead>
            <tbody>
                {% for resource in data %}
                    <tr>
                        <td>{{ resource.description }}</td>
                        <td align="right">             
                            <a href="manager/resource/form/{{ resource.id }}"><div class="basic ui icon button"><i class="edit icon"></i></div></a>
                            <a href="manager/resource/delete/{{ resource.id }}"><div class="basic ui icon button"><i class="remove icon"></i></div></a>
                        </td>
                    </tr>
                {% endfor %}
            </tbody>
        </table>

    </div>
</div>

<script type="text/javascript">
    $(function() {
        $('.recursos').addClass('active');
    });
</script>