<div class="thirteen wide column">
    <h3 class="body-title-page" data-menu=".usuarios" style="margin-bottom:30px">
        <a href="manager/user" title="Voltar para a página anterior"><i class="large angle left icon"></i></a>Novo usuário
    </h3>
    {{ flashSession.output() }}
    <form action="manager/user/save" method="post" class="ui form">
        <div class="ui segment form-bg">
            <div class="ui error message"></div>
            <input name="id" value="{{ row.id }}" type="hidden">
            <div class="field">
                <label>Nome Completo</label>
                <div class="ui fluid labeled input">
                    <input id='name' name="name" value="{{ row.name }}" type="text">
                    <div class="ui corner label">
                        <i class="icon asterisk"></i>
                    </div>                            
                </div>
            </div>
            <div class="two fields">
                <div class="field">
                    <label>Usuário</label>
                    <div class="ui left labeled input">
                        <input id='identity' name="identity" value="{{ row.identity }}" type="text">
                        <div class="ui corner label">
                            <i class="icon asterisk"></i>
                        </div>
                    </div>
                </div>
                <div class="field">
                    <label>E-mail</label>
                    <div class="ui left labeled input">
                        <input id="email" name="email" value="{{ row.email }}" type="text">
                        <div class="ui corner label">
                            <i class="icon asterisk"></i>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <!-- ---------------------------- Propiedades ----------------------------------  -->
        <div class="ui">
            <h5 class="ui header">Propriedades</h5>
        
            {% for index,property in properties %}
                {% set adapter = property['struct']['adapter'] %}
                {% set value = null %}
                <div class="field">
                    <label>{{ property['struct']['name'] }}</label>
                    <div class="ui fluid labeled input">
                        
                        {% set key = property['struct']['nativeName'] %}
                        
                        {% if adapter == 'sisrel' %}
                            {% set key = property['struct']['reference'] %}
                        {% endif %}
                        
                        {% if adapter == 'form' %}
                            {% set key = property['struct']['field'] %}
                        {% endif %}
                        
                        <?php if (isset($userProperties[$adapter][$key])): ?>
                            <?php $value = $userProperties[$adapter][$key];?>
                        <?php endif ?>
                        
                        <input class="tags" id="tag_{{ index }}" name="properties[{{ adapter }}][{{ key }}]" type="text" value="{{ value }}">
                        <div style="clear: both"></div>
                    </div>
                </div>
            {% endfor %}
        </div>

        <!-- ---------------------------- Sistemas ----------------------------------  -->
        <div class="ui">
            <h5 class="ui header">Sistemas e perfis</h5>

            <div class="ui fluid accordion">
                {% for system in systems %}
                <div class="title">
                    <i class="dropdown icon"></i>
                    {{ system.name }}
                </div>
                <div class="content">
                    {% for data in userRoles if system.id == data.system_id %}
                        <div class="field">
                            <div class="ui toggle checkbox">
                                <input id="role_{{data.role_id}}-system_{{data.system_id}}" name="userRoles[][{{data.role_id}}|{{system.id}}|{{row.id}}]" type="checkbox" {{data.role_checked}}>
                                <label for="role_{{data.role_id}}-system_{{data.system_id}}">{{data.role_name}}</label>
                            </div>      
                        </div>
                    {% endfor %}
                </div>
                {% endfor %}                
            </div>
            <div class="ui segment basic floated right" style="margin:0;padding:0">
                <a href="#" class="link-text">voltar</a> &nbsp;
                {% if attempt == true %}
                <a href="manager/user/unlock/{{ row.id }}" class="ui icon button"><i class="unlock alternate icon"></i> Desbloquear</a>
                {% endif %}
                <button type="submit" class="ui icon button"><i class="save icon"></i> Salvar</button>
            </div>
        </div>
    </form>
</div>

<script type="text/javascript">
    $(function() {
        
        $('.usuarios').addClass('active');
        
        {% for index, property in properties %}
            <?php $options = (new Manager\Adapter\Factory($property['struct'], $di))->getOptions(null) ?>
            
            var phpOptions = {{options|json_encode}},
                options = [];

            $.each(phpOptions, function(i, value){
                options.push({id:i, value:value, label:value});
            });
            $('#tag_{{index}}').tagsInput({
                width: 'auto',
                height: 'auto',
                autocomplete_url: options,
                /* onChange: function(elem, elem_tags) {
                    console.log($(this));
                    $('.tag', elem_tags).each(function(){
                        if ($(this).data('added')) {
                            return;
                        }
                        
                        $(this).prepend('aaaa');
                        $(this).data('added', true);
                    });
				},*/
                interactive: true,
                placeholderColor: '#ffffff'
            });
        {% endfor %}        
  
    });
    
    $('.ui.form').form({
        name: {
            identifier  : 'name',
            rules: [{
                type   : 'empty',
                prompt : 'Campo de preencimento obrigatório'
            }]
        },
        identity: {
            identifier  : 'identity',
            rules: [{
                type   : 'empty',
                prompt : 'Campo de preencimento obrigatório'
            }]
        },
        email: {
            identifier  : 'email',
            rules: [{
                type   : 'empty',
                prompt : 'Campo de preencimento obrigatório'
            }]
        }
    });  
</script>