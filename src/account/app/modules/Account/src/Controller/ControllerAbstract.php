<?php

namespace Account\Controller;


class ControllerAbstract extends \Intelletto\Webservice\Mvc\Controller
{
    public function initialize()
    {
        $this->view->cdnUrl = $this->config->cdn->url;
        $this->view->cdnTheme = $this->config->cdn->theme;
        $this->view->userName = $this->session->get('identity')['userName'];
        $this->flashSession->setCssClasses([
            'success' => 'ui small green message', 
            'error' => 'ui small red message'
        ]);        
    }
}