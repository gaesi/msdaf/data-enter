<?php

namespace Account\Model;

use Phalcon\Mvc\Model;
use Phalcon\Mvc\Model\Resultset\Simple as Resultset;

class Property extends Model 
{
    public $id;
    public $key;
    public $value;

    public function initialize() 
    {
        $this->setSchema("public");
        $this->setSource("property");
    }
    
    public static function findAllJson($id=null)
    {
        if(isset($id)){
            $sql = "SELECT id, key, string_agg(trim(j_value::text, '\"'), ', ') AS value
                    FROM   property p, json_array_elements(p.value) j(j_value)
                    WHERE  p.id = ".$id."
                    GROUP BY p.id";
        } else {
            $sql = "SELECT id, key, string_agg(trim(j_value::text, '\"'), ', ') AS value
                    FROM   property p, json_array_elements(p.value) j(j_value)
                    GROUP BY p.id";            
        }
        
        $model = new Property();
         
        return new Resultset(null, $model, $model->getReadConnection()->query($sql));        
    }
}