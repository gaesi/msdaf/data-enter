<?php

namespace Account\Model;

use Phalcon\Mvc\Model;

class System extends Model 
{
    public $id;
    public $name;
    public $return_url;
    public $key;
    public $secret;
    public $visible;

    public function initialize() 
    {
        $this->setSchema("public");
        $this->setSource("system");
 
        $this->hasMany('id', "Manager\Model\RoleResource", "system_id",array('alias'=>'resources'));
    }
}