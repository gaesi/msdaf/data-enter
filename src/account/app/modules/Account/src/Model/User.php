<?php

namespace Account\Model;

use Phalcon\Mvc\Model;
use Phalcon\Mvc\Model\Resultset\Simple as Resultset;

class User extends Model 
{
    public $id;
    public $name;
    public $email;
    public $identity;
    public $secret;
    public $property;

    public function initialize() 
    {
        $this->setSchema("public");
        $this->setSource("user");
    }
    
    public static function findAllProperties()
    {
 
        // melhorar o sql
        //
        $sql = "SELECT p.key, trim(element::text, '\"') element
                FROM   property p, json_array_elements(p.value->'data') element;";            

        $model = new User();
         
        return new Resultset(null, $model, $model->getReadConnection()->query($sql));
                
    }    
}