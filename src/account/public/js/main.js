
$(function() {

    $('.ui.dropdown').dropdown();

    var modal = $('#poi-modal');
    $('#map-column').click(function() {
        modal.modal('show');
    });

    $('.tabular.menu .item').tab();

    // map
    var $map = $('#map-column');
    if ($map && $map.length) {
        var resizeMap = function() {
            var totalHeight = $(window).height();
            var fixedHeight = 0;
            $('.ui.fixed.menu').each(function() {
                fixedHeight += $(this).height();
            });
            $map.css('height', totalHeight - fixedHeight);
        };
        resizeMap();
        $(window).on('resize', resizeMap);
    }

    $('.ui.accordion')
      .accordion()
    ;  

});
