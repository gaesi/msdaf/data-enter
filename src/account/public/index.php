<?php
define('APPLICATION_ENV', getenv('APPLICATION_ENV'));
define('APPLICATION_PATH', realpath(__DIR__ . '/../app/'));

require '../vendor/autoload.php';

use Intelletto\Bootstrap\Bootstrap;
use Phalcon\DI\FactoryDefault,
    Phalcon\Config\Adapter\Ini;

$config = new Ini(APPLICATION_PATH . '/configs/app.ini');
$app = new Bootstrap(new FactoryDefault(), $config, APPLICATION_ENV);
echo $app->run();