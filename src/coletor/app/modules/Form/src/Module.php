<?php

namespace Form;

use Intelletto\Webservice\Service\Authentication;
use Intelletto\Webservice\Service\Acl;
use Intelletto\Webservice\Service\Form;
use Intelletto\Webservice\Service\Data;
use Intelletto\Webservice\Service\File;
use Intelletto\Webservice\Service\Group;
use Intelletto\Webservice\Service\System;
use Intelletto\Webservice\Service\Role;
use Intelletto\Webservice\Service\User;

class Module extends \Intelletto\Bootstrap\Module
{
	public function registerServices($di)
	{
        parent::registerServices($di);
        
        $di->set('authentication', function() use ($di) {
            return new Authentication($di->get('webservice'));
        });
        
        $di->set('aclService', function() use($di) {
            return new Acl($di->get('webservice'));
        });
        
        $di->set('form', function() use($di) {
            return new Form($di->get('webservice'));
        });
        
        $di->set('group', function() use($di) {
            return new Group($di->get('webservice'));
        });
        
        $di->set('dataService', function() use($di) {
            return new Data($di->get('webservice'));
        });
        
        $di->set('file', function() use($di) {
            return new File($di->get('webservice'));
        });
        
        $di->get('flashSession')->setCssClasses([
            'success' => 'ui small green message', 
            'error' => 'ui small red message'
        ]);
            
        $di->set('system', function() use ($di) {
            return new System($di->get('webservice'));
        });

        $di->set('user', function() use($di) {
            return new User($di->get('webservice'));
        });

        $di->set('role', function() use($di) {
            return new Role($di->get('webservice'));
        });
        $userId = $di['session']->identity['userId'];
        $di['view']->listSystems = $di->get('user')->systems($userId)->getData();

        $lastAccess = $di->get('user')->lastAccess($userId)->getData()['created'];
        $di['view']->lastAccess = date('d/m/Y H:i:s', strtotime($lastAccess));

        $roles = $di->get('role')->byUserAndSystem($userId, 5)->getData();
        $di['view']->listRoles = $roles;

        $di['view']->redirect_system = $di['config']->idp->url;

        $di['view']->appname = $di['config']->view->appname;

        $di['view']->roleId = $di['session']->identity['role_id'];
        $di['view']->userName = $di['session']->identity['userName'];
        setcookie('http_x_forwarded_host', $di['config']->sisrel->http_x_forwarded_host, time() + 14400,'/');
        setcookie('perfil_cookie', base64_encode($di['config']->sisrel->perfil_cookie), time() + 14400, '/');
        setcookie('co_usuario', $di['view']->userId, time() + 14400, '/');
	}
}