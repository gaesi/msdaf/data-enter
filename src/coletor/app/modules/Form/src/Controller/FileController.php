<?php

namespace Form\Controller;

class FileController extends \Intelletto\Webservice\Mvc\Controller
{   
    public function initialize()
    {
        $this->view->header = "data";
    }
    
    public function indexAction($formId)
    {
        $response     = $this->file->all($formId);
        $formResponse = $this->form->get($formId);
        $data         = $response->getData()['file'];
        $formData     = $formResponse->getData()['form'];
        
        $this->view->form    = $formData;
        $this->view->groupId = $formData['group_id'];
        $this->view->fields  = json_decode($formData['struct']);
        $this->view->data    = $data;
        $this->view->forms   = $this->getForms();
        $this->view->groups  = $this->getGroups();
    }
    
    public function templateAction($formId)
    {
        header('Content-Description: File Transfer');
        header('Content-Type: text/csv');
        header('Content-Disposition: attachment; filename=modelo.csv');
        header('Expires: 0');
        header('Cache-Control: must-revalidate');
        header('Pragma: public');
        
        $formResponse = $this->form->get($formId);
        
        $struct = json_decode($formResponse->getData()['form']['struct']);
        
        echo implode(';', array_map(function($value){
            return $value->nativeName;
        }, $struct));
        
        echo "\n";
        
        echo implode(',', array_map(function($value){
            $sample = '';
            
            switch ($value->type) {
                case 'date':
                    $sample = date('d/m/Y');
                    break;
                case 'decimal':
                    $sample = '"12,34"';
                    break;
                case 'file':
                    $sample = "caminho/do/arquivo.jpg";
                    break;
                case 'map':
                    $sample = "-15.7801,-47.9292";
                    break;
                case 'number':
                    $sample = 1;
                    break;
                case 'text':
                    $sample = "Texto livre";
                    break;
                case 'options':
                case 'form':
                    $sample = "(verificar opções no formulário)";
                    break;
            }
            
            return $sample;
        }, $struct));
        
        exit;
    }
    
    public function uploadAction($formId)
    {
        if (! $this->request->hasFiles()) {
            $this->flashSession->error('Arquivo inválido.');
            return $this->response->redirect('form/file/index/'. $formId);
        }
        
        $file = $this->request->getUploadedFiles()[0];

        if (! $this->isValidFile($file)) {
            $this->flashSession->error('Arquivo inválido. Envie somente arquivos .csv');
            return $this->response->redirect('form/file/index/'. $formId);
        }
        
        $filename = $this->getUniqueName($file->getName());
        $path     = APPLICATION_PATH . '/../data/upload/' . $filename;
        
        if (move_uploaded_file($file->getTempName(), $path)) {
            $response = $this->file->import($formId, $path);
            
            if ($response->isSuccess()) {
                $this->flashSession->success('Arquivo enviado com sucesso.');
                return $this->response->redirect('form/file/index/' . $formId);
            }
        }
        
        $this->flashSession->error('Arquivo inválido.');
        return $this->response->redirect('form/file/index/'. $formId);
    }
    
    protected function getForms()
    {
        return $this->form->all()->getData()['form'];
    }
    
    protected function getGroups()
    {
        return $this->group->all()->getData()['group'];
    }
    
    protected function isValidFile($file)
    {
        $pathinfo = pathinfo($file->getName());
        
        if ($pathinfo['extension'] == 'csv') {
            return true;
        }
        
        return false;
    }

    protected function getUniqueName($name)
    {
        $pathinfo = str_replace(' ','_',pathinfo($name));
        $prefix   = preg_replace("/[^a-zA-Z0-9]+/", "", $pathinfo['filename']);
        $unique   = uniqid($prefix . '-');
        return $unique . '.' . $pathinfo['extension'];
    }
}