<?php
namespace Form\Controller;


class IndexController extends \Intelletto\Webservice\Mvc\Controller
{
    public function indexAction()
    {
        if ($this->acl->isAllowed('/form/form/index')) {
            return $this->response->redirect('form/form/index');
        }
        
        return $this->response->redirect('form/data/index');
    }
}
