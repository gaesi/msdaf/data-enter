<?php

namespace Form\Controller;

use Imagick;

class DataController extends \Intelletto\Webservice\Mvc\Controller
{
    public function initialize()
    {
        $this->view->header = "data";
    }

    public function indexAction($groupId = 0)
    {
        $groups = $this->getGroups();

        if (! $groupId) {
            $groupId = $groups[0]['id'];
        }

        $response = $this->form->getByGroup($groupId);

        $group = array_filter($groups, function($value) use ($groupId) {
            return ($groupId == $value['id']);
        });

        $this->view->forms   = $response->getData()['form'];
        $this->view->groups  = $groups;
        $this->view->group   = current($group);
        $this->view->groupId = $groupId;
    }

    public function viewAction($groupId, $formId)
    {
        $groups = $this->getGroups();
        $limit  = $this->request->get('limit', 'int', 30);
        $offset = $this->request->get('offset', 'int', 0);
        $query  = trim($this->request->get('q', 'string'));
        $order  = trim($this->request->get('order', 'string'));
        $sorted=null;
        $fieldOrder=null;
        $arrOrder=null;
        if( !empty($order)) {
            $arrOrder   = explode(' ', $order);
            $fieldOrder = $arrOrder[0];
            $sorted     = $arrOrder[1];
        }

        $properties   = (array) json_decode($this->session->identity['property']);
        $formResponse = $this->form->get($formId);
        $formData     = $formResponse->getData()['form'];
        $fields       = json_decode($formData['struct']);

        $response = $this->dataService->setQuery($query)
            ->setFilters($properties, $formData)
            ->setOrder($order)
            ->setOffset($offset)
            ->setLimit($limit)
            ->all($formId);

        $data  = $response->getData()['data'];
        $total = $response->getData()['total'];

        $this->view->groupId = $groupId;
        $this->view->groups  = $groups;
        $this->view->form    = $formData;
        $this->view->fields  = $fields;
        $this->view->data    = $data;
        $this->view->total   = $total;
        $this->view->query   = $query;
        $this->view->offset  = $offset;
        $this->view->limit   = $limit;
        $this->view->sorted  = $sorted;
        $this->view->fieldOrder  = $fieldOrder;
        $this->view->pages   = ceil($total/$limit) - 1;
    }

    public function formAction($formId, $id = 0)
    {
        $formResponse = $this->form->get($formId);
        $data = ['id' => 0];

        if ($id) {
            $response = $this->dataService->get($id);
            $data     = $response->getData()['data'];

            $this->view->value = (array) json_decode($data['value']);
        }

        $formData = $formResponse->getData()['form'];
        $fields   = json_decode($formData['struct']);

        $this->view->groups   = $this->getGroups();
        $this->view->groupId  = $formData['group_id'];
        $this->view->form     = $formData;
        $this->view->fields   = $fields;
        $this->view->data     = $data;
        $this->view->di       = $this->getDi();
        $this->view->filesize = ini_get('upload_max_filesize');
        $this->view->maxsize  = ini_get('post_max_size');
    }

    public function saveAction()
    {
        $optionSave = (int) $this->request->getPost('btnSalvar');
        $value      = $this->request->getPost('data');
        $id         = (int) $this->request->getPost('id');
        $groupId    = (int) $this->request->getPost('group_id');
        $formId     = (int) $this->request->getPost('form_id');
        $form       = $this->form->get($formId);
        $struct     = json_decode($form->getData()['form']['struct']);

        $this->uploadFiles($value);

        $data = ['value' => json_encode($value), 'form_id' => $formId, 'user_id' => $this->getDi()['session']->identity['userId']];

        if ($id) {
            $response = $this->dataService->update($id, $data);
        } else {
            $response = $this->dataService->insert($data);
            $id = $response->getData()['data']['id'];
        }

        if ($response->isError()) {
            $this->flashSession->error('Falha ao salvar.');
            $this->flashSession->error($response->getErrorMessage());
            return $this->response->redirect(sprintf('form/data/form/%d/%d', $formId, $id));
        }

        $this->flashSession->success('Salvo com sucesso.');

        if($optionSave == 1) {
            return $this->response->redirect(sprintf('form/data/view/%d/%d', $groupId, $formId));
        }

        if($optionSave == 2) {
            return $this->response->redirect(sprintf('form/data/form/%d', $formId));
        }
    }

    public function fileAction($filename,$thumb=null)
    {
        $resolutionQuality=72;
        $maxLandWidth=1024;
        $maxPortHeight=768;
        if($thumb){
            $resolutionQuality=16;
            $maxLandWidth=200;
            $maxPortHeight=150;
        }
        $path = APPLICATION_PATH . '/../data/upload/' . basename($filename);
        $pathtmp = APPLICATION_PATH . '/../data/upload/'.$resolutionQuality.$resolutionQuality.'_'. basename($filename);
        if (file_exists($path)) {
            if(@is_array(getimagesize($path))){
                if (!file_exists($pathtmp)) {
                    $image = new Imagick($path);
                    $resolution = $image->getImageResolution();
                    $imgHeight = $image->getImageHeight();
                    $imgWidth = $image->getImageWidth();
                    if ($resolution['x'] > $resolutionQuality) {
                        $image->setImageUnits(imagick::RESOLUTION_PIXELSPERINCH);
                        $image->setResolution($resolutionQuality, $resolutionQuality);
                        $image->resampleImage($resolutionQuality, $resolutionQuality, imagick::FILTER_UNDEFINED, 1);
                        if($thumb){
                            $image->setImageCompressionQuality(70);
                        }
                        $imgHeight = $image->getImageHeight();
                        $imgWidth = $image->getImageWidth();
                        $path = $pathtmp;
                    }
                    if ($imgHeight>$imgWidth) {
                        if($imgHeight>$maxPortHeight){http://idp.localhost/idp/login/index/3
                            $image->resizeImage(($imgWidth*$maxPortHeight)/$imgHeight,$maxPortHeight,Imagick::FILTER_LANCZOS,1);
                            $path = $pathtmp;
                        }
                    }else{
                        if($imgWidth>$maxLandWidth){
                            $image->resizeImage($maxLandWidth,($maxPortHeight*$imgHeight)/$imgWidth,Imagick::FILTER_LANCZOS,1);
                            $path = $pathtmp;
                        }
                    }
                    $image->writeImage($pathtmp);
                    $image->clear();
                    $image->destroy();

                }else{
                    $path = $pathtmp;
                }
            }
            header('Content-Description: File Transfer');
            header('Content-Type: application/octet-stream');
            header('Content-Disposition: attachment; filename='.basename($path));
            header('Expires: 0');
            header('Cache-Control: must-revalidate');
            header('Pragma: public');
            header('Content-Length: ' . filesize($path));
            ob_clean();
            flush();
            readfile($path);

            exit;
        }

        exit('Arquivo inválido.');
    }

    public function loadAction()
    {
        $this->view->setRenderLevel(\Phalcon\Mvc\View::LEVEL_ACTION_VIEW);

        $value = $this->request->getPost('value');
        $field = (array) $this->request->getPost('field');

        $this->view->lastDataValue = $value;
        $this->view->field = $field;
        $this->view->di = $this->getDi();
    }

    public function deleteAction($groupId, $formId, $id)
    {
        $response = $this->dataService->delete($id);
        $url = sprintf('form/data/view/%s/%s', $groupId, $formId);


        if ($response->isError()) {
            $this->flashSession->error('Registro inválido.');
            return $this->response->redirect($url);
        }

        $this->flashSession->success('Realizado com sucesso.');
        return $this->response->redirect($url);
    }

    protected function getUniqueName($name)
    {
        $pathinfo = pathinfo($name);
        $prefix   = preg_replace("/[^a-zA-Z0-9_]+/", "", str_replace('-','_', str_replace(' ','_',$pathinfo['filename']) ) );
        $unique   = uniqid($prefix . '-');
        return $unique . '.' . $pathinfo['extension'];
    }

    protected function uploadFiles(&$value)
    {
        if (! (isset($_FILES['data']) && current(current($_FILES['data']['name'])))) {
            return false;
        }

        foreach ($_FILES['data']['name'] as $key => $names) {
            foreach ($names as $keyName => $name) {
                $filename = $this->getUniqueName($name);
                $path     = APPLICATION_PATH . '/../data/upload/' . $filename;

                if (move_uploaded_file($_FILES['data']['tmp_name'][$key][$keyName], $path)) {
                    $value[$key][] = $filename;
                }
            }
        }
    }

    protected function getForms()
    {
        return $this->form->all()->getData()['form'];
    }

    protected function getGroups()
    {
        return $this->group->all()->getData()['group'];
    }
}