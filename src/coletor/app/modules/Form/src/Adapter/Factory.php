<?php

namespace Form\Adapter;

class Factory
{
    protected $adapter;
    
    public function __construct($struct, $di = null)
    {
        switch ($struct['adapter']) {
            case 'static':
                $adapter = new StaticOption($struct);
                break;
            case 'sisrel':
                $adapter = new Sisrel($struct);
                $adapter->setDi($di);
                break;
            case 'form':
                $adapter = new Form($struct);
                $adapter->setDi($di);
                break;
        }
        
        $this->adapter = $adapter;
    }
    
    public function getOptions($parentValue)
    {
        return $this->adapter->getOptions($parentValue);
    }
}