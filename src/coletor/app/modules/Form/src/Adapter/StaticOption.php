<?php

namespace Form\Adapter;

class StaticOption implements AdapterInterface
{
    protected $options;
    
    public function __construct(array $struct)
    {
        $this->options = $struct['options'];
    }
    
    public function getOptions($parentValue)
    {
        $options = explode(",", $this->options);
        $data = ['' => "(em branco)"];
        
        foreach ($options as $value) {
            $data[$value] = $value;
        }
        
        return $data;
    }
}