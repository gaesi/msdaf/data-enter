<?php

namespace Form\Adapter;

use Phalcon\DI\FactoryDefault;

class Form implements AdapterInterface
{
    protected $field;
    protected $filter;
    protected $dynamic;
    protected $di;

    public function __construct(array $struct)
    {
        $this->field   = $struct['field'];
        $this->dynamic = isset($struct['dynamic']) ? $struct['dynamic'] : false;
        $this->filter  = isset($struct['filter']) ? $struct['filter'] : false;
    }

    public function setDI(FactoryDefault $di)
    {
        $this->di = $di;
    }

    public function getOptions($parentValue)
    {
        $data     = ['' => "(em branco)"];
        $parts    = explode('.', $this->field);
        $formId   = $parts[0];
        $field    = $parts[1];

        if (!$parentValue && $this->dynamic) {
            return ['' => "(aguardando seleção)"];
        }

        $response = $this->di['dataService']->setOrder($field)->setLimit(1000)->setJoin(false)->all($formId);
        $rows     = $response->getData()['data'];
        foreach ($rows as $row) {
            $value = json_decode($row['value']);

            if ($this->filter && (
                    (isset($value->{$this->filter}) && $value->{$this->filter} != $parentValue) ||
                    ($this->filter == 'id' && $row['id'] != $parentValue)
                )
            ){
                continue;
            }
            if (isset($value->$field)) {
                $data[$row['id']] = $value->$field;
            }
        }
        return $data;
    }
}