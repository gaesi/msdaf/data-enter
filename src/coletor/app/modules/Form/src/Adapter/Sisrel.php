<?php

namespace Form\Adapter;

use Phalcon\DI\FactoryDefault;
use Form\Service\Sisrel as Service;

class Sisrel implements AdapterInterface
{
    protected $reference;
    protected $filter;
    protected $dynamic;
    protected $di;
    
    public function __construct(array $struct)
    {
        $this->reference = $struct['reference'];
        $this->dynamic = isset($struct['dynamic']) ? $struct['dynamic'] : false;
        $this->filter  = isset($struct['filter']) ? $struct['filter'] : false;
    }
    
    public function setDI(FactoryDefault $di)
    {
        $this->di = $di;
    }
    
    public function getOptions($parentValue)
    {
        $data = ['' => "(em branco)"];
        
        if (!$parentValue && $this->dynamic) {
            return ['' => "(aguardando seleção)"];
        }
        
        $parts = explode('.', $this->reference);
        $query = [
            'co_consulta' => $parts[0], 
            'field' => $parts[1]
        ];
        
        if ($this->dynamic && $parentValue) {
            $query[$this->filter] = $parentValue;
        }
        
        $rows = (new Service)->getColValues($this->di['config']->sisrel->url, $query);
        
        if (! count($rows)) {
            return $data;
        }
        
        foreach ($rows as $row) {
            if ($row['valor'] != null && $row['valor'] != '') {
                $data[$row['valor']] = $row['valor'];
            }
        }
        
        return $data;
    }
}