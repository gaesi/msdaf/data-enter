{% include 'data/menu.volt' %}
<div class="column wide thirteen">
    <h3 class="body-title-page" style="margin-bottom:30px">
        <a href="form/data/view/{{groupId}}/{{form['id']}}" title="Voltar para a página anterior"><i class="large angle left icon"></i></a>{{ form['name'] }}
    </h3>
    <form class="ui form segment input-upload" id="file-upload" action="form/file/upload/{{ form['id'] }}" method="post" enctype="multipart/form-data">
        <div class="ui error message"></div>
        <div class="two fields">
            <div class="field">
                <div class="ui input">
                    <input id="file" name="file" type="file">
                    <div class="ui corner label">
                        <i class="icon asterisk"></i>
                    </div>
                </div>
            </div>
            <div class="field">
                <button type="submit" class="ui icon button"><i class="upload disk icon"></i> Importar</button>
            </div>
        </div>
    </form>
    <table class="ui table small segment table-margin">
        <thead>
            <tr>
                <th>Nome</th>
                <th>Situação</th>
                <th>Data de importação</th>
                <th>Última modificação</th>
                <th></th>
            <tr>
        </thead>
        <tbody>
            {% if data|length %}
                {% for row in data %}
                    <tr>
                        <td>{{ row['name'] }}</td>
                        <td>
                            {{ row['status'] }} 
                        </td>
                        <td><?php echo date('d/m/Y H:i', strtotime($row['created'])) ?></td>
                        <td><?php echo date('d/m/Y H:i', strtotime($row['updated'])) ?></td>
                        <td width="90"><a href="#" class="detail basic ui button mini">detalhar</a></td>
                    </tr>
                    <tr style="display: none;">
                        <td colspan="5">{{ row['detail'] }}</td>
                    </tr>
                {% endfor %}
            {% else %}
                <tr><td colspan="5">Nenhum arquivo importado.</td></tr>    
            {% endif %}
        </tbody>
    </table>
    
    <div class="ui icon small floating message default-message">
        <i class="download disk icon"></i>
        <div class="content">
            <div class="header default-message-title">
                Modelo para importação
            </div>
            <p>
                Utilize uma planilha modelo já existente para importar os seus dados.
                <a class="ui small label" href="form/file/template/{{ form['id'] }}">Baixar modelo</a>
            </p>
        </div>  
    </div>
</div>
<script type="text/javascript">
    $('#file-upload').form({
        file: {
          identifier: 'file',
          rules: [{type: 'empty', prompt : 'Selecione um arquivo'}]
        }
    });
    
    $('.detail').click(function(){
        $(this).parent().parent().next().toggle();
        return false;
    });
</script>