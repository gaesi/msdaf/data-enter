<form class="row" id="formBuild" action="form/form/save" method="POST" style="margin:0">
    <div class="column wide sixteen">
        <h3 class="body-title-page">
            <a href="form/form/index" title="Voltar para a página anterior"><i class="large angle left icon"></i></a>
            {% if data['id'] %}
                Editar formulário
            {% else %}
                Novo formulário
            {% endif %}

            {% include '../../../layouts/message.volt' %}
            <div class="ui compact menu small right floated">
                <div class="menu">
                    <div class="ui top right pointing dropdown link item">
                        <i class="icon key"></i> Perfis de acesso <i class="dropdown icon"></i>
                        <div class="menu key-users">    
                            {% for role in roles if role['visible'] or role['name'] == "Suporte" %}
                                {% set checked = null %}
                                {% if role['id'] in data['roles'] %}{% set checked = 'checked' %}{% endif %}
                                <div class="ui checkbox">
                                    <input name="roles[]" {{ checked }} type="checkbox" value="{{ role['id'] }}">
                                    <label>{{ role['name'] }}</label>
                                </div>
                            {% endfor %}            
                        </div>
                    </div>
                </div>
            </div>
        </h3>     
    </div>

    <div class="sixteen wide column" style="padding-top: 1.4em">
    	<div class="ui form segment form-bg">
            <div class="ui two fields">
                <div class="field">
                    <label>Nome do formulário</label>
                    <div class="ui left labeled input">
                        <input value="{{ data['name'] }}" type="text" name="formName" placeholder="Nome do formulário">
                        <div class="ui corner label">
                            <i class="icon asterisk"></i>
                        </div>
                    </div>
                </div>
                <div class="field">
                    <label>Layout</label>
                    <div class="ui selection fluid dropdown">
                        <input name="layout" type="hidden" value="{{ data['layout'] }}">
                        <div class="default text">Layout</div>
                        <i class="dropdown icon"></i>
                        <div class="menu">
                            <div class="item" data-value="vertical">Vertical</div>
                            <div class="item" data-value="auto">Automático</div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    
    <div class="four wide column" style="padding-top: 1.4em">
        <h5 class="ui top attached header">Campos</h5>
    	<div id="origin" class="ui attached segment stacked types">
            {% for type,field in metadata %}
        		<div class="ui raised segment">
        			<i class="{{ field['icon'] }} icon"></i> {{ field['label'] }} <span class="addicional"></span>
                    <input class="type" type="hidden" name="type" value="{{ type }}">
        		</div>
                <script type="text/html" id="template-element-{{ type }}">
                    <?php $field = (object) ['type' => $type, 'nativeType' => $field['native'], 'name' => '', 'nativeName' => '', 'length' => '', 'description' => '']; ?>
                    {% set i = 0 %}
                    {% set isEditing = false %}
                    {{ partial('form/box-field') }}
                </script>
            {% endfor %}
    	</div>
    </div>
        
    <div class="twelve wide column">
        {% if data['id'] %} {% set isEditing = true %} {% endif %}
        <input type="hidden" name="id" value="{{ data['id'] }}">
        <input type="hidden" name="group_id" value="{{ data['group_id'] }}">
    	<br>
        <div id="elements" class="ui segment basic" style="margin-top: 0; padding: 0;">
            {% if fields|length %}
        		{% for i,field in fields %}
                    {{ partial('form/box-field') }}
                {% endfor %}
                <h3 style="display: none;">Solte os campos aqui</h3> 
            {% else %}
               <h3>Solte os campos aqui</h3> 
            {% endif %}
    	</div>
        <div class="ui segment basic floated right">
            <a href="form/form/index" class="link-text">voltar</a> &nbsp; 
            {% if data['id'] %}
                <a class="ui button basic delete icon" href="form/form/delete/{{ data['id'] }}"><i class="remove icon"></i> Excluir</a>
            {% endif %}
            <button type="submit" class="ui icon button"><i class="save icon"></i> Salvar</button>
        </div>
    </div>
</form>

<div  class="column wide sixteen"></div>

<script type="text/javascript" src="js/functions.js?v=1.2"></script>
<script type="text/javascript" src="js/form.js?v=1.3"></script>