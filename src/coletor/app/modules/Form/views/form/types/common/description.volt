<div class="field">
    <label>Descrição</label>
    <div class="ui left labeled input">
        <input value="{{ field.description }}" name="fields[][{{ i }}][description]" placeholder="Descrição" type="text">
    </div>
</div>