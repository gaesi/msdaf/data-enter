<div class="field name">
    <label>Nome</label>
    <div class="ui left labeled input">
        <input maxlength="64" value="{{ field.name }}" class="name" name="fields[][{{ i }}][name]" placeholder="Nome" type="text">
        <div class="ui corner label">
            <i class="icon asterisk"></i>
        </div>
    </div>
</div>