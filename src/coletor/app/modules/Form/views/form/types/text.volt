<div class="ui form">
    <div class="ui two fields">
        
        {{ partial('form/types/common/name') }}
        
        <div class="field length">
            <label>Tamanho</label>
            <div class="ui left labeled input">
                <input value="{{ field.length }}" name="fields[][{{ i }}][length]" placeholder="Tamanho" type="text">
                <div class="ui corner label">
                    <i class="icon asterisk"></i>
                </div>
            </div>
        </div>
    </div>
    
    {{ partial('form/types/common/description') }}
    <div class="ui two fields">
        {{ partial('form/types/common/checks') }}
    </div>
</div>