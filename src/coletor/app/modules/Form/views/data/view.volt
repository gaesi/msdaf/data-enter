{% include 'data/menu.volt' %}
<div class="column wide thirteen">
    <h3 class="body-title-page" style="margin-bottom:30px;">
        <a href="form/data/index/{{groupId}}" title="Voltar para a página anterior"><i class="large angle left icon"></i></a>{{ form['name'] }}
    </h3>
    {% include '../../../layouts/message.volt' %}
    <div class="ui" style="margin-bottom:30px;  margin-top: 30px;">
        <a href="form/data/form/{{ form['id'] }}" class="ui small right floated submit button"><i class="add icon"></i> Novo</a>
        <a href="form/file/index/{{ form['id'] }}" class="ui small right floated submit button"><i class="upload disk icon"></i> Importar</a>
        <form action="" method="get" accept-charset="utf-8">
            <div class="ui action small input">
                <input name="q" placeholder="Procurar..." type="text" value="{{ query }}">
                <div class="ui small icon button"><i class="search icon"></i></div>
                <div class="ui small label" style="margin-left:15px;background:none">
                    Total de registros
                    <div class="detail">{{total}}</div>
                </div>
            </div>
        </form>
    </div>
    <table class="ui basic table small" style="margin-bottom:30px">
        <thead>
            <tr>
                {% for i,field in fields %}
                    {% if i == 6 %}{% break %}{% endif %}
                        {% if field.nativeName == fieldOrder %}
                            {% if sorted == 'ASC' %}
                                {% set reverseSorted = 'DESC' %}
                                {% set class = 'headerSortDown' %}
                            {% else %}
                                {% set reverseSorted = 'ASC' %}
                                {% set class = 'headerSortUp' %}
                            {% endif %}
                            <th class="{{class}}"><a href="form/data/view/{{ groupId }}/{{ form['id'] }}/?offset=0&limit={{ limit }}&q={{ query }}&order={{ field.nativeName }} {{reverseSorted}}">{{ field.name }}</a></th>
                        {% else %}
                            <th class="header"><a href="form/data/view/{{ groupId }}/{{ form['id'] }}/?offset=0&limit={{ limit }}&q={{ query }}&order={{ field.nativeName }} ASC">{{ field.name }}</a></th>
                        {% endif %}
                {% endfor %}
                <th></th>
            <tr>
        </thead>
        <tbody>
            {% if data|length %}
                {% for row in data %}
                    <tr>
                        {% for i,field in fields %}
                            {% if i == 6 %}{% break %}{% endif %}
                            <?php $value = (array) json_decode($row['value']); ?>
                            <td>
                                {% if value[field.nativeName] is defined %}
                                    {% set dataValue = value[field.nativeName] %}
                                
                                    {% if field.nativeType == 'timestamp' %}
                                        <?php $dataValue = date('d/m/Y', strtotime($dataValue)) ?>
                                    {% endif %}
                    
                                    {% if field.nativeType == 'numeric' %}
                                        <?php $dataValue = number_format((float) $dataValue, 2, ',', '.') ?>
                                    {% endif %}
                                    {% if field.nativeType == 'integer' %}
                                        <?php $dataValue = number_format((float) $dataValue, 0, '', '.') ?>
                                    {% endif %}
                                
                                    {{ dataValue }}     
                                {% endif %}
                            </td>
                        {% endfor %}
                        <td width="120" style="text-align:right">
                            <a href="form/data/form/{{ row['form_id'] }}/{{ row['id'] }}" class="link-icon"><i class="edit link icon"></i></a>
                            <a href="form/data/delete/{{groupId}}/{{ row['form_id'] }}/{{ row['id'] }}" class="link-icon"><i class="delete link icon"></i></a>
                        </td>
                    </tr>
                {% endfor %}
            {% else %}
                <tr>
                    <td colspan="6">Nenhum registro cadastrado.</td>
                </tr>
            {% endif %}
        </tbody>
    </table>
    {% include '../../../layouts/modal-delete.volt' %}
    
    {% if total %}

        {% if not sorted is empty and not fieldOrder is empty %}
            {% set order = "&order="~fieldOrder ~" "~ sorted%}
        {% else %}
            {% set order = "&order=" %}
        {% endif %}

        <div  style="text-align:center;font-size:12px">
            <div class="ui small pagination menu" style="box-shadow:none">
                <a {% if offset > 0 %} href="form/data/view/{{ groupId }}/{{ form['id'] }}/?offset=0&limit={{ limit }}&q={{ query }}{{order}}" {% endif %} class="icon item" title="Primeira página">
                    <i class="left arrow icon"></i>
                    <i class="left arrow icon"></i>
                </a>
            </div>
            <div class="ui small pagination menu" style="margin-right:10px;box-shadow:none">
                <a {% if offset > 0 %} href="form/data/view/{{ groupId }}/{{ form['id'] }}/?offset={{ offset - limit }}&limit={{ limit }}&q={{ query }}{{order}}" {% endif %} class="icon item" title="Página anterior">
                    <i class="left arrow icon"></i>
                </a>
            </div>
            
            {% set page = offset/limit %}
            
			Página &nbsp;&nbsp;
			
            <select id="page">
                {% for index in 0..pages %}
                    <option {% if index == page %} selected {% endif %} value="form/data/view/{{ groupId }}/{{ form['id'] }}/?offset={{ index * limit }}&limit={{ limit }}&q={{ query }}{{order}}">
                        {{index + 1}}
                    </option>
                {% endfor %}
            </select>
            &nbsp;&nbsp; de &nbsp;&nbsp; <strong>{{ pages + 1 }}</strong>

            <div class="ui small pagination menu" style="margin-left:10px;box-shadow:none">
                <a {% if offset < total - limit %} href="form/data/view/{{ groupId }}/{{ form['id'] }}/?offset={{ offset + limit }}&limit={{ limit }}&q={{ query }}{{ order }}" {% endif %} class="icon item" title="Próxima página">
                    <i class="right arrow icon"></i>
                </a>
            </div>
            <div class="ui small pagination menu" style="box-shadow:none">
                <a {% if offset < total - limit %} href="form/data/view/{{ groupId }}/{{ form['id'] }}/?offset={{ pages * limit }}&limit={{ limit }}&q={{ query }}{{ order }}" {% endif %} class="icon item" title="Última página">
                    <i class="right arrow icon"></i>
                    <i class="right arrow icon"></i>
                </a>
            </div>
        </div>
    {% endif %}
</div>
<script type="text/javascript">

    $('#page').change(function(){
        location = this.value;
    });
</script>