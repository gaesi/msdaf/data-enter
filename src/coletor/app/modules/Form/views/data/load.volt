<?php $options = (new Form\Adapter\Factory($field, $di))->getOptions($lastDataValue) ?>
{% for index,option in options %}
    <div class="item" data-value="{{ index }}">{{ option }}</div>
{% endfor %}