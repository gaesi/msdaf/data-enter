<!DOCTYPE html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js"> <!--<![endif]-->
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>{{appname}}</title>
    <meta name="description" content="">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <base href="{{ url() }}">
    <link rel='stylesheet' type='text/css' href='http://fonts.googleapis.com/css?family=Source+Sans+Pro:400,700|Open+Sans:300italic,400,300,700'>
    {{ assets.outputCss('cssHeader') }}
    <link rel="stylesheet" type="text/css" href="css/form.css?v=1.2">

    <script type="text/javascript" src="http://maps.googleapis.com/maps/api/js?sensor=false"></script>
    {{ assets.outputJs('jsHeader') }}
    <script type="text/javascript" src="js/moment-with-locales.min.js"></script>

    <script type="text/javascript" src="js/plugins.js"></script>
    <script type="text/javascript" src="js/main.js"></script>

</head>

<body>

<div class="ui main fixed menu">
    <div class="container">
        <div class="title item">
            {{ logo }}
        </div>

        <!-- MENU USUÁRIO -->
        <div id="dropdown-config-account" class="ui large top right right floated pointing dropdown item current user">
            <div class="text user-logado">{{ userName }}</div>
            <i class="triangle down large icon"></i>
            <div class="ui menu user-config">
                <div class="text item ultimo-acesso">
                    <small>
                        Último acesso:
                        <em>{{ lastAccess }}</em>
                    </small>
                </div>
                {% for system in listSystems %}
                {% if system['id'] == 5 %}
                <div class="item"><i class="user icon"></i><a href="{{ redirect_system }}idp/login/index/{{system['id']}}" style="text-decoration:none">Configurações da conta</a></div>
                {% endif %}
                {% endfor %}
                <div class="header item"><i class="key icon"></i>Perfis</div>
                <div class="lista-perfis">
                    {% for role in listRoles %}
                    <a href="form/index/changeProfile/{{ role['id'] }}" title="{{ role['id'] ==  roleId ? 'Perfil atual' : '' }}" class="{{ role['id'] ==  roleId ? 'current-profile' : '' }}">{{role['name']}}</a>
                    {% endfor %}
                </div>
                <div class="item actions">
                    <a class="ui mini button right floated" href="form/index/logout">Sair</a>
                </div>
            </div>
        </div>

        <!-- MENU MÓDULOS -->
        <div class="ui large top right icon right floated pointing dropdown button modulos">
            <i class="setting icon"></i>
            <div class="ui menu lista-modulos">
                {% for system in listSystems %}
                    {% if system['id'] == 1 %}
                    <div class="item"><i class="tasks icon"></i><a href="{{ redirect_system }}idp/login/index/{{system['id']}}" style="text-decoration:none">{{system['name']}}</a></div>
                    {% elseif system['id'] == 2 %}
                    <div class="item"><i class="dashboard icon"></i><a href="{{ redirect_system }}idp/login/index/{{system['id']}}" style="text-decoration:none">Sala de situação</a></div>
                    {% elseif system['id'] == 3 %}
                    <div class="item"><i class="tasks icon"></i><a href="{{ redirect_system }}idp/login/index/{{system['id']}}" style="text-decoration:none">Administração</a></div>
                    {% elseif system['id'] == 4 %}
                    <div class="item"><i class="puzzle piece icon"></i><a href="{{ redirect_system }}idp/login/index/{{system['id']}}" style="text-decoration:none">Gestão de dados</a></div>
                    {% elseif system['id'] == 7 %}
                    <div class="item"><i class="recursos-hidricos icon"></i><a href="{{ redirect_system }}idp/login/index/{{system['id']}}" style="text-decoration:none">Monitoramento Hidrológico</a></div>
                    {% endif %}
                {% endfor %}
            </div>
        </div>
    </div>
</div>

<div class="ui segment basic page-margin header-adm">
    <h3 class="ui header left floated header-title">Gestão de dados</h3>
    <div class="ui secondary menu small right floated header-menu-adm">
        {% if acl.isAllowed('/form/form/index') %}
        <a href="form/form/index" class="{% if header == "form" %}active{% endif %} item"><i class="browser icon"></i> Formulários</a>
        {% endif %}

        {% if acl.isAllowed('/form/form/index') %}
        <a href="form/data/index" class="{% if header == "data" %}active{% endif %} item"><i class="table icon"></i> Conteúdo</a>
        {% endif %}
    </div>
</div>

<div class="ui grid layout admin">
    {{ content() }}
</div>

</body>
</html>



