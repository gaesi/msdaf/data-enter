<?php

namespace Intelletto\Bootstrap;

use Phalcon\Config\Adapter\Ini;
use Phalcon\DI;
use Phalcon\Loader;
use Phalcon\Mvc\Application;
use Phalcon\Debug;
use Phalcon\Assets\FilterInterface;
use Phalcon\Assets\Filters\None;
use Phalcon\Assets\Filters\Cssmin;
use Phalcon\Assets\Filters\Jsmin;


class Bootstrap extends Application
{
    /**
     * The variable indicates the application environment
     * @var string|null
     */
    private $environment;

    /**
     * Application configuration
     * @var Config|null
     */
    private $config;

    /**
     * The loader of namespace
     * @var Loader|null
     */
    private $loader;

    /**
     * @param DI $di
     * @param null|string $environment
     */
    public function __construct(DI $di, Ini $config, $environment)
    {
        parent::__construct($di);

        $di->get('session')->start();

        $this->config = $config;
        $this->environment = $environment;

        if ($environment == 'development') {
            (new Debug())->listen(true, true);
        }

        $this->getDi()->set('config', $config);
    }

    /**
     * Running the application
     * @return string
     */
    public function run()
    {
        $this->boot();
        $this->initModules();
        $this->initServices();
        $this->initAssets();

        return $this->handle()->getContent();
    }

    /**
     * Initializing
     */
    protected function boot()
    {
        $path = APPLICATION_PATH . '/configs/' . $this->environment . '.ini';
        if (is_readable($path)) {
            $ini = new Ini($path);
            $this->config->merge($ini);
        }

        if ($this->loader === null) {
            $this->loader = new Loader();
        }
    }

    /**
     * Initializing modules
     */
    protected function initModules()
    {
        $modules = $this->config->get('modules', ['active' => ['Core']]);
        $register = [];

        foreach ($modules['active'] as $module) {
            $moduleRouterName = \Phalcon\Text::uncamelize($module);
            $register[$moduleRouterName] = [
                'className' => sprintf('%s\Module', $module),
                'path' => sprintf('%s/modules/%s/src/Module.php', APPLICATION_PATH, $module)
            ];

            $namespace = sprintf("%s", $module);
            $path = sprintf('%s/modules/%s/src', APPLICATION_PATH, $module);

            $this->loader->registerNamespaces([$namespace => $path], true);
        }

        $this->loader->register();
        $this->registerModules($register);
    }

    /**
     * Initializing namespace of application
     */
    protected function initServices()
    {
        $defaultServices  = ['view', 'dispatcher', 'router'];
        $iniServices      = array_keys($this->config->toArray());
        $services         = array_unique(array_merge($defaultServices, $iniServices));
        $defaultNamespace = 'Intelletto\\Bootstrap\\Service\\';
        $di               = $this->getDi();

        foreach ($services as $serviceName) {
            $className = $defaultNamespace . ucfirst(strtolower($serviceName));

            if (! class_exists($className)) {
                continue;
            }

            $service = new $className($this->config->get($serviceName));
            $di->set($serviceName, $service->getService($di));
        }
    }
    protected  function initAssets()
    {
        $options=$this->getDI()->get('config');
        if(!isset($options->view->cdn) || !$options->view->cdn['enable']){
            return;
        }

        $this->assets->collection('cssHeader')->addCss($options->view->cdn['url'].'/themes/'.$options->view->cdn['theme'].'/tmp/finalRemote.css',false);
        $this->assets->collection('jsHeader')->addJs($options->view->cdn['url'].'/themes/'.$options->view->cdn['theme'].'/tmp/finalRemote.js',false);

        try
        {
            $temp = tempnam('/tmp', 'cdn-');
            file_put_contents($temp,file_get_contents($options->view->cdn['url'].'/themes/'.$options->view->cdn['theme'].'/app.ini'));
        }catch (Exception $e){
            var_dump($e);
        }
        $config = new \Phalcon\Config\Adapter\Ini($temp);
        unlink($temp);

        foreach($config->element->file as $keyElement=>$element){
            $this->view->$keyElement=str_replace('$cdn.url',$options->view->cdn['url'],file_get_contents($options->view->cdn['url'].'/themes/'.$options->view->cdn['theme'].'/'.$element));
        }
    }
}