<?php

namespace Intelletto\Bootstrap\Service;

abstract class ServiceAbstract implements ServiceInterface
{
    protected $options;
    
    public function __construct($options = null)
    {
        $this->options = $options;
    }
    
    public function setOption($name, $value)
    {
        $this->options[$name] = $value;
    }
    
    public function setOptions(array $options)
    {
        $this->options = $options;
    }
}