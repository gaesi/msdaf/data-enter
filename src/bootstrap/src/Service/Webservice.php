<?php

namespace Intelletto\Bootstrap\Service;

use GuzzleHttp\Client as Guzzle;
use Intelletto\Webservice\Client;
use GuzzleHttp\Adapter\StreamAdapter;
use GuzzleHttp\Message\MessageFactory;
use Phalcon\DI;

class Webservice extends ServiceAbstract
{
    public function getService(DI $di)
    {
        $options = $this->options;
        
        return function() use ($options, $di) {
            $httpClient = new Guzzle(['adapter' => new StreamAdapter(new MessageFactory)]);
            $client     = new Client($options->toArray(), $httpClient, $di->get('session'));
            
            return $client;
        };
    }
}