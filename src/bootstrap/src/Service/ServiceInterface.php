<?php

namespace Intelletto\Bootstrap\Service;

use Phalcon\DI;

interface ServiceInterface
{
    public function getService(DI $di);
}