<?php

namespace Intelletto\Bootstrap\Service;

use Phalcon\DI;

class Db extends ServiceAbstract
{
    public function getService(DI $di)
    {
        $options = $this->options;

        return function() use ($options) {
            $className = "\Phalcon\Db\Adapter\Pdo\\" . $options['adapter'];
            return new $className([
                'host' => $options['host'],
                'username' => $options['username'],
                'password' => $options['password'],
                'dbname' => $options['dbname']
            ]);
        };
    }
}