<?php

namespace Intelletto\Bootstrap\Service;

use Phalcon\DI;

class View extends ServiceAbstract
{
    public function getService(DI $di)
    {
        $options = $this->options;

        return function() use ($options) {
            $view = new \Phalcon\Mvc\View();
            
            $view->setViewsDir($options['viewsDir']);
            $view->setLayoutsDir($options['layoutsDir']);
            $view->setTemplateAfter('layout');
            
            $view->registerEngines([
                '.volt' => function ($view, $di) use ($options) {
                    $volt = new \Phalcon\Mvc\View\Engine\Volt($view, $di);
                    $volt->setOptions(array(
                        'compiledPath' => $options['cacheDir'],
                        'compiledSeparator' => '_',
                        'compileAlways' => true 
                    ));
                    return $volt;
                }
            ]);

            return $view;
        };
    }
}