<?php


namespace Intelletto\Bootstrap\Service;

use Phalcon\DI;

class Acl extends ServiceAbstract
{
    protected $di;
    
    public function getService(DI $di)
    {
        $this->di = $di;
        $options = $this->options;

        return function() {
            return $this;
        };
    }
    
    public function isAllowed($resource)
    {
        $openResources = isset($this->options['open']) ? $this->options['open']->toArray() : [];
        $acl = $this->di->get('aclService');
        $session = $this->di->get('session');
        $idp = $this->di->get('config')->idp;
        
        if (in_array($resource, $openResources)) {
            return true;
        }
        
        if ($session->has('identity') && $session->get('identity')) {
            $response = $acl->isAllowed($resource);
            
            if ($response->isError()) {
                return false;
            }
            
            $data = $response->getData();
            
            if ($data['acl']['access']) {
                return true;
            }
            
            return false;
        }
        if(isset($_SERVER['REDIRECT_URL'])){
            $session->set('dennyLastAcess',$_SERVER['REDIRECT_URL']);
        }
        $url = $idp->url . 'idp/login/index/' . $idp->system_id;
        
        header('Location: '. $url); 
        exit;
    }
}