<?php

namespace Intelletto\Bootstrap\Service;

use Phalcon\Mvc\Dispatcher as MvcDispatcher,
    Phalcon\Events\Manager as EventsManager;
use Phalcon\DI;

class Dispatcher extends ServiceAbstract
{
    public function getService(DI $di)
    {
        $options = $this->options;
        
        return function () use ($options) {
            $dispatcher = new MvcDispatcher();
            $eventsManager = new EventsManager();

            $eventsManager->attach("dispatch:beforeDispatchLoop", function($event, $dispatcher) {
                $resource = sprintf('/%s/%s/%s', 
                    $dispatcher->getModuleName(),
                    $dispatcher->getControllerName(),
                    $dispatcher->getActionName()
                );
                        
                $acl = $dispatcher->getDI()->get('acl');
                
                if (! $acl->isAllowed($resource)) {
                    $defaultModule = strtolower($dispatcher->getDi()->get('config')->modules->active->toArray()[0]);
                    $dispatcher->forward([
                        'module' => $defaultModule, 
                        'controller' => 'error', 
                        'action' => 'access'
                    ]);
                }
                $module = \Phalcon\Text::camelize($dispatcher->getModuleName());
                $namespace = sprintf("%s\Controller\\", $module);
                $dispatcher->setDefaultNamespace($namespace);
            });
            
            $dispatcher->setEventsManager($eventsManager);

            return $dispatcher;
        };
    }
}