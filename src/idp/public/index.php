<?php
function x( $vars )
{
    echo "<div id='x'>";
    foreach ( func_get_args() as $idx => $arg ) {
        echo "<b><u>ARG[$idx]</u></b><br><pre>";
        print_r( $arg );
        echo "</pre>";
    }
    echo "</div><br><br>";
    flush();
}
define('APPLICATION_ENV', getenv('APPLICATION_ENV'));
define('APPLICATION_PATH', realpath(__DIR__ . '/../app/'));

require '../vendor/autoload.php';

use Intelletto\Bootstrap\Bootstrap;
use Phalcon\DI\FactoryDefault,
    Phalcon\Config\Adapter\Ini;

$config = new Ini(APPLICATION_PATH . '/configs/app.ini');
$app = new Bootstrap(new FactoryDefault(), $config, APPLICATION_ENV);
echo $app->run();