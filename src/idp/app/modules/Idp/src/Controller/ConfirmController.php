<?php

namespace Idp\Controller;

class ConfirmController extends \Phalcon\Mvc\Controller 
{
    public function indexAction() 
    {
        $token = $this->request->get('token');
        $response = $this->authentication->confirm($token);
        
        if ($response->isError()) {
            $this->flashSession->error($response->getErrorMessage());
            $this->response->redirect('idp/index');
            return false;
        }
        
        $this->flashSession->success('Cadastro confirmado.');
        $this->response->redirect('idp/index');
    }
}