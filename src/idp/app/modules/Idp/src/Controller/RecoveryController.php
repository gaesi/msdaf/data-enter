<?php

namespace Idp\Controller;

class RecoveryController extends \Phalcon\Mvc\Controller 
{
    public function indexAction() 
    {}
        
    public function sendAction() 
    {
        if (! $this->request->isPost()) {
            $this->response->redirect('idp/recovery');
            return false;
        }
        
        $email = $this->request->getPost('email');
        $response = $this->authentication->forgot($email);

        if ($response->isError()) {
            $this->flashSession->error($response->getErrorMessage());
            $this->response->redirect('idp/index');
            return false;
        }

        //dados do usuario
        $userData = $response->getData();

        $this->sendMail($userData);

        $this->flashSession->success("Enviamos um e-mail para {$userData['user']['email']} com as instruções de como alterar a senha.");
        $this->response->redirect('idp/index');
        return true;
    }

    protected function sendMail($data)
    {
        // @todo Verificar possibilidade de utilizar o partials via template do phalcon
        $user = $data['user'];

        $identity = $user['identity'];
        $secret   = $data['secret'];

        $emailBody = "Você está recebendo este e-mail porque solicitou trocar sua senha.<br>";
        $emailBody.= "Para concluir este procedimento com segurança, acesse o sistema com a senha temporária informada abaixo,";
        $emailBody.= " clique no menu \"Configurações da conta\" e acesse a opção \"Alterar senha\".<br><br>";
        $emailBody.= "Login: $identity<br>";
        $emailBody.= "Senha: <b style='color: red;'>$secret</b><br><br><br>";
        $emailBody.= "Endereço: <a href='".$this->config->idp->url."'>".$this->config->idp->url."</a><br><br>";
        $emailBody.= "Caso não tenha solicitado a troca de senha, execute o procedimento para sua segurança.";
        $this->mailService->send($user['email'],'Recuperar Senha', $emailBody);
    }
}