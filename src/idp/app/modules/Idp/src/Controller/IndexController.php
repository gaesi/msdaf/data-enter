<?php

namespace Idp\Controller;

use Idp\Model\System;

class IndexController extends \Phalcon\Mvc\Controller
{
    public function indexAction()
    {
        $dashboardId = $this->config->idp->dashboard_id;
        return $this->response->redirect('idp/login/index/' . $dashboardId);
    }

}