<?php

namespace Idp;

use Intelletto\Webservice\Service\Authentication;
use Intelletto\Webservice\Service\Acl;
use Intelletto\Webservice\Service\System;
use Intelletto\Webservice\Service\User;
use Intelletto\Webservice\Service\Role;
use Intelletto\Webservice\Service\Mail;

class Module extends \Intelletto\Bootstrap\Module
{
	public function registerServices($di)
	{
        parent::registerServices($di);
        
        $di->set('authentication', function() use ($di) {
            return new Authentication($di->get('webservice'));
        });
        
        $di->set('aclService', function() use ($di) {
            return new Acl($di->get('webservice'));
        });
        
        $di->set('role', function() use ($di) {
            return new Role($di->get('webservice'));
        });

        $di->set('system', function() use ($di) {
            return new System($di->get('webservice'));
        });

        $di->set('user', function() use ($di) {
            return new User($di->get('webservice'));
        });

        $di->set('mailService', function() use($di) {
            return new Mail($di->get('webservice'));
        });
        
        $di->get('flashSession')->setCssClasses([
            'success' => 'ui small green message', 
            'error' => 'ui small red message'
        ]);
        $di['view']->appname = $di['config']->view->appname;
	}
}