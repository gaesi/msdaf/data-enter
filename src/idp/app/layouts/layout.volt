<!DOCTYPE html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js"> <!--<![endif]-->
    <html>
        <head>
            <!-- Standard Meta -->
            <meta charset="utf-8" />
            <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
            <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0">
            <base href="{{ url() }}">

            <!-- Site Properities -->
            <title>{{appname}}</title>

            <meta name="description" content="">
            <meta name="viewport" content="width=device-width, initial-scale=1">

            <link href='http://fonts.googleapis.com/css?family=Source+Sans+Pro:400,700|Open+Sans:300italic,400,300,700' rel='stylesheet' type='text/css'>
            {{ assets.outputCss('cssHeader') }}
            {{ assets.outputJs('jsHeader') }}
        </head>
        <body class="bkg-idp">
            <div class="ui main fixed menu">
                <div class="title item">
                    {{ logo }}
                </div>
            </div>

            <div class="ui three column page grid doubling" style="margin-top: 50px;">

                <div class="column"></div>
                <div class="column">{{ content() }}</div>
                <div class="column"></div>
            </div>
        </body>
    </html>