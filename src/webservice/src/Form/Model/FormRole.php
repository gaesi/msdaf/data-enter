<?php

namespace Webservice\Form\Model;

use Phalcon\Mvc\Model;

class FormRole extends Model 
{
    public function initialize() 
    {
        $this->setConnectionService('dbData');
        
        $this->setSchema("public");
        $this->setSource("form_role");
        
        $this->belongsTo(
            'form_id', 
            'Webservice\Form\Model\Form', 
            'id', 
            ['alias' => 'form']
        );
    }
}