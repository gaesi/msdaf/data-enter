<?php

namespace Webservice\Form\Model;

use Phalcon\Mvc\Model;

class Form extends Model 
{
    public function initialize() 
    {
        $this->setConnectionService('dbData');
        
        $this->setSchema("public");
        $this->setSource("form");
        
        $this->hasMany('id', "Webservice\Form\Model\Data", "form_id");
        
        $this->belongsTo(
            'group_id', 
            'Webservice\Form\Model\Group', 
            'id', 
            ['alias' => 'group']
        );
        
        $this->skipAttributesOnCreate(['created','updated','deleted']);
    }
    
    public function beforeUpdate()
    {
        $this->updated = date('Y-m-d H:i:s');
    }
}