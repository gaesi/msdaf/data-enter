<?php

namespace Webservice\Form\Model;

use Phalcon\Mvc\Model;

class Group extends Model 
{
    public function initialize() 
    {
        $this->setConnectionService('dbData');
        
        $this->setSchema("public");
        $this->setSource("group");
        
        $this->hasMany('id', "Webservice\Form\Model\Form", "group_id");
        
        $this->skipAttributesOnCreate(['deleted']);
    }
    
    public function beforeUpdate()
    {
        $this->updated = date('Y-m-d H:i:s');
    }
}