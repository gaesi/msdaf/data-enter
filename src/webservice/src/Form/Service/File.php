<?php

namespace Webservice\Form\Service;

use Webservice\Form\Model\File as FileModel;
use Webservice\Service\ServiceAbstract;

class File extends ServiceAbstract
{
    protected $total;
    
    public function getTotal() 
    {
        return $this->total;
    }
    
    public function getAll($formId)
    {
        $condition = 'form_id = ?0 AND deleted = 0';
        $bind      = [$formId];
        
        $params = [
            'conditions' => $condition,
            'bind' => $bind,
            'columns' => ['id, form_id, created, updated, detail, status, name']
        ];
        
        $this->total = FileModel::count($params);
        
        return FileModel::find($params);
    }
    
    public function getPendingFiles()
    {
        return FileModel::find("status = 'Enviado'");
    }
    
    public function insert($data)
    {
        unset($data['id']);
        $model = new FileModel;
        
        if (! $model->save($data)) {
            $this->errorMessage = current($model->getMessages());
            return false;
        }
        
        return $model->id;
    }
    
    public function isValidFile($file)
    {
        $pathinfo = pathinfo($file->getName());
        
        if ($pathinfo['extension'] == 'csv') {
            return true;
        }
        
        return false;
    }
}