<?php

namespace Webservice\Form\Service;

use Webservice\Form\Model\Form as FormModel;
use Webservice\Service\ServiceAbstract;
use Webservice\Form\Model\FormRole;

class Form extends ServiceAbstract
{
    public function getAll($groupId = null)
    {
        $conditions = 'deleted = 0';
        $bind = [];
        
        if ($groupId) {
            $conditions = 'deleted = 0 AND group_id = ?0';
            $bind = [$groupId];
        }
        
        return FormModel::find([
            'conditions' => $conditions,
            'bind' => $bind,
            'columns' => ['id, name, group_id, created, updated, struct'],
            'order' => 'id DESC'
        ]);
    }
    
    public function getById($id, $isForView = false)
    {
        if ($isForView) {
            return FormModel::find([
                'conditions' => 'id = ?0 AND deleted = 0',
                'bind' => [$id],
                'columns' => ['id, name, group_id, created, updated, struct, layout']
            ])[0];
        }
        
        return FormModel::findFirst($id);
    }

    public function countByGroup($groupId)
    {
        return FormModel::count([
            'conditions' => 'group_id = ?0 AND deleted = 0', 
            'bind' => [$groupId]
        ]);
    }
    
    public function insert($data)
    {
        unset($data['id']);
        $model = new FormModel;
        
        if (! $model->save($data)) {
            $this->errorMessage = current($model->getMessages());
            return false;
        }
        
        $this->saveRoles($model->id, $data);
        
        return $model->id;
    }
    
    public function update($id, $data)
    {
        if (! $id) {
            $this->errorMessage = 'O id do formulário deve ser informado.';
            return false;
        }
        
        $model = $this->getById($id);
        
        if (! $model) {
            $this->errorMessage = 'Formulário inválido.';
            return false;
        }
        
        if (! $model->save($data)) {
            $this->errorMessage = current($model->getMessages());
            return false;
        }
        
        $this->saveRoles($id, $data);
        
        return true;
    }
    
    protected function saveRoles($formId, $data)
    {
        FormRole::findByFormId($formId)->delete();
        
        if (isset($data['roles']) && count($data['roles'])) {
            foreach ($data['roles'] as $role) {
                $formRole = new FormRole;
                $formRole->save([
                    'form_id' => $formId,
                    'role_id' => $role
                ]);
            }
        }
    }
    
    public function delete($id)
    {
        $model = $this->getById($id);
        
        if (! $model) {
            $this->errorMessage = 'Formulário inválido.';
            return false;
        }
        
        $model->deleted = 1;
        
        if (! $model->save()) {
            $this->errorMessage = current($model->getMessages());
            return false;
        }
        
        return true;
    }
}