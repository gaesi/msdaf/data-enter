<?php

namespace Webservice\Service;

abstract class ServiceAbstract
{
    protected $di;
    protected $errorMessage;

    public function getErrorMessage()
    {
        return $this->errorMessage;
    }
    
    public function getDI()
    {
        return $this->di;
    }
    
    public function setDI($di)
    {
        $this->di = $di;
    }
}