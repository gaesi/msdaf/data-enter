<?php

namespace Webservice\Mvc;

use Phalcon\Db\Adapter\Pdo\Postgresql;
use Phalcon\Config\Adapter\Ini;
use Phalcon\DI\FactoryDefault;
use Phalcon\Logger\Adapter\File as FileAdapter;
use Webservice\Translator;
use Webservice\Oauth\Storage\ClientStorage as Client;
use Webservice\Oauth\Storage\SessionStorage as Session;
use Webservice\Oauth\Storage\ScopeStorage as Scope;
use Webservice\Oauth\Storage\AccessTokenStorage as AccessToken;
use League\OAuth2\Server\ResourceServer;
use League\OAuth2\Server\AuthorizationServer;
use League\OAuth2\Server\Grant\ClientCredentialsGrant as ClientCredentials;
use Illuminate\Database\Capsule\Manager as Capsule;

class Micro extends \Phalcon\Mvc\Micro 
{
    public function setConfig($filename, $env) 
    {
        $this->setDI(new FactoryDefault());
        
        $config = new Ini($filename);

        $path = APPLICATION_PATH . '/config/' . $env . '.ini';
        if (is_readable($path)) {
            $ini = new Ini($path);
            $config->merge($ini);
        }
        
        $this['config'] = $config;
    
        $this['db'] = function() {    
            return new Postgresql(array(
                'host' => $this['config']->db->host,
                'username' => $this['config']->db->username,
                'password' => $this['config']->db->password,
                'dbname' => $this['config']->db->dbname,
            ));
        };
        
        $this['dbData'] = function() {
            return new Postgresql([
                'host' => $this['config']->dbData->host,
                'username' => $this['config']->dbData->username,
                'password' => $this['config']->dbData->password,
                'dbname' => $this['config']->dbData->dbname
            ]);
        };

        $this['translator'] = function() {
            return Translator::factory($this['config']->translator->toArray());
        };
        
        $this['logger'] = function() {    
            return new FileAdapter($this['config']->logger->filename);
        };
        
        $this['oauth_db'] = function(){
            $capsule = new Capsule();

            $capsule->addConnection([
                'driver'   => 'pgsql',
                'host'     => $this['config']->db->host,
                'username' => $this['config']->db->username,
                'password' => $this['config']->db->password,
                'database' => $this['config']->db->dbname,
                'charset'  => 'utf8'
            ]);

            $capsule->setAsGlobal();
            
            return $capsule;
        };
        
        $this['oauth_resource'] = function() {
            $this['oauth_db'];
            return new ResourceServer(new Session, new AccessToken, new Client, new Scope);
        };
        
        $this['oauth_server'] = function() {
            $this['oauth_db'];

            $server = new AuthorizationServer();
            $server->setClientStorage(new Client);
            $server->setSessionStorage(new Session);
            $server->setScopeStorage(new Scope);
            $server->setAccessTokenStorage(new AccessToken);

            $clientCredentials = new ClientCredentials();
            $server->addGrantType($clientCredentials);

            return $server;
        };
    }

    public function handle($uri = null) 
    {
        $this->setRoutes();
        
        $this->after(function() {
            $jsend = $this->getReturnedValue();
            
            $this->response->setHeader("Content-Type", "application/json");    
            $this->response->setContent($jsend->encode());
            $this->response->send();
        });

        parent::handle($uri);
    }

    private function setRoutes() 
    {
        $sql       = "SELECT * FROM resource WHERE controller IS NOT NULL AND action IS NOT NULL";
        $resources = $this->db->fetchAll($sql, \Phalcon\Db::FETCH_ASSOC);
        
        if (! count($resources)) {
            throw new \InvalidArgumentException('Requisição inválida');
        }
        
        foreach ($resources as $row) {
            if (! (class_exists($row['controller']) && $row['controller'])) {
                continue;
            }
            
            $this['logger']->log(print_r($row, true));
            
            call_user_func_array([$this, $row['method']], [
                $row['route'], [new $row['controller'], $row['action']]
            ]);
        }
    }
}