<?php

namespace Webservice\Oauth\Controller;

use Phalcon\Mvc\Controller;
use Webservice\Mvc\Response;

class Token extends Controller 
{
    public function index()
    {
        $key    = $this->request->getPost('key');
        $secret = $this->request->getPost('secret');
        
        $response = $this->oauth_server->issueAccessToken([
            'client_id' => $key,
            'client_secret' => $secret
        ]);
        
        return Response::success([
            'oauth' => $response
        ]);
    }   
}