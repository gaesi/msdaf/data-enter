<?php

namespace Webservice\Oauth\Storage;

use Illuminate\Database\Capsule\Manager as Capsule;
use League\OAuth2\Server\Entity\ClientEntity;
use League\OAuth2\Server\Entity\SessionEntity;
use League\OAuth2\Server\Storage\AbstractStorage;
use League\OAuth2\Server\Storage\ClientInterface;

class ClientStorage extends AbstractStorage implements ClientInterface
{
    /**
     * {@inheritdoc}
     */
    public function get($clientId, $clientSecret = null, $redirectUri = null, $grantType = null)
    {
        $query = Capsule::table('system')
                          ->select('system.*')
                          ->where('system.key', $clientId);

        if ($clientSecret !== null) {
            $query->where('system.secret', $clientSecret);
        }

        if ($redirectUri) {
            $query->join('oauth_client_redirect_uris', 'system.key', '=', 'oauth_client_redirect_uris.client_id')
                  ->select(['system.*', 'oauth_client_redirect_uris.*'])
                  ->where('oauth_client_redirect_uris.redirect_uri', $redirectUri);
        }

        $result = $query->get();

        if (count($result) === 1) {
            $client = new ClientEntity($this->server);
            $client->hydrate([
                'id'    =>  $result[0]['key'],
                'name'  =>  $result[0]['name'],
            ]);

            return $client;
        }

        return null;
    }

    /**
     * {@inheritdoc}
     */
    public function getBySession(SessionEntity $session)
    {
        $result = Capsule::table('system')
                            ->select(['system.key', 'system.name'])
                            ->join('oauth_sessions', 'system.key', '=', 'oauth_sessions.client_id')
                            ->where('oauth_sessions.id', $session->getId())
                            ->get();

        if (count($result) === 1) {
            $client = new ClientEntity($this->server);
            $client->hydrate([
                'id'    =>  $result[0]['key'],
                'name'  =>  $result[0]['name'],
            ]);

            return $client;
        }

        return null;
    }
}
