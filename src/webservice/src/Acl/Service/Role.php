<?php

namespace Webservice\Acl\Service;

use Webservice\Acl\Model\UserRole;
use Webservice\Acl\Model\RoleResource;
use Webservice\Acl\Model\Resource;
use Webservice\Acl\Model\UserLastRole;
use Webservice\Acl\Model\Role as RoleModel;
use Webservice\Service\ServiceAbstract;

class Role extends ServiceAbstract
{
    public function getAll()
    {
        return RoleModel::find();
    }
    
    public function getIdByName($name)
    {
        return RoleModel::findFirstByName($name)->id;
    }
    
    public function getByUserAndSystem($userId, $systemId)
    {
        $data = [];
        $roles = UserRole::find([
            "conditions" => "user_id = ?0 AND system_id = ?1",
            "bind"       => [$userId, $systemId]
        ]);
        
        foreach ($roles as $role) {
            $data[]  = RoleModel::findFirst($role->role_id)->toArray();
        }
        
        return $data;
    }
    
    public function last($userId, $systemId)
    {
        $data = [];
        $roles = UserLastRole::find([
            "conditions" => "user_id = ?0 AND system_id = ?1",
            "bind"       => [$userId, $systemId],
            "order"      => 'id DESC'
        ]);
            
        if (count($roles)) {
            return $roles[0]->role_id;
        }
        
        return 0;
    }
    
    public function validateAccess($userId, $systemId, $roleId)
    {
        $data = [];
        $roles = UserRole::find([
            "conditions" => "user_id = ?0 AND system_id = ?1 AND role_id = ?2",
            "bind"       => [$userId, $systemId, $roleId]
        ]);
        
        if (count($roles)) {
            (new UserLastRole)->save([
                'user_id' => $userId,
                'role_id' => $roleId,
                'system_id' => $systemId,
                'created' => new \Phalcon\Db\RawValue('default')
            ]);
            return true;
        }
        
        return false;
    }
    
    public function validateResourceAccess($roleId, $route)
    {
        $resource = Resource::findFirstByRoute($route);
        
        if (! $resource) {
            return false;
        }
        
        $data = RoleResource::find([
            "conditions" => "resource_id = ?0 AND role_id = ?1",
            "bind"       => [$resource->id, $roleId]
        ]);
        
        if (count($data)) {
            return true;
        }
        
        return false;
    }
}