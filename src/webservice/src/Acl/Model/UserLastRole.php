<?php

namespace Webservice\Acl\Model;

use Phalcon\Mvc\Model;

class UserLastRole extends Model 
{
    public $user_id;
    public $role_id;

    public function initialize() 
    {
        $this->setSchema("public");
        $this->setSource("user_last_role");

        $this->belongsTo(
            'user_id', 
            'Webservice\Common\Model\User',
            'id', 
            array('alias' => 'user')
        );
                
        $this->belongsTo(
            'role_id', 
            'Webservice\Acl\Model\Role',
            'id', 
            array('alias' => 'role')
        );
    }
}