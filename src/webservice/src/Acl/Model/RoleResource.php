<?php

namespace Webservice\Acl\Model;

use Phalcon\Mvc\Model;

class RoleResource extends Model 
{
    public $resource_id;
    public $role_id;

    public function initialize() 
    {
        $this->setSchema("public");
        $this->setSource("role_resource");

        $this->belongsTo(
            'resource_id', 
            'Webservice\Acl\Model\Resource',
            'id', 
            array('alias' => 'resource')
        );
                
        $this->belongsTo(
            'role_id', 
            'Webservice\Acl\Model\Role',
            'id', 
            array('alias' => 'role')
        );
    }
}