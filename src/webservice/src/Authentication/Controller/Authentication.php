<?php

namespace Webservice\Authentication\Controller;

use Phalcon\Mvc\Controller;
use Webservice\Authentication\Service\Login;
use Webservice\Common\Model\User as ModelUser;
use Webservice\Authentication\Model\LoginToken;
use Webservice\Common\Controller\Mail;
use Webservice\Mvc\Response;

class Authentication extends Controller 
{
    public function login()
    {
        $identity  = $this->request->getPost('identity');
        $secret    = $this->request->getPost('secret');
        $ip        = getallheaders()['X_AUTH_IP']; //$this->request->getHeader('X_AUTH_IP');
        $userAgent = getallheaders()['X_AUTH_HTTP_USER_AGENT']; //$this->request->getHeader('X_AUTH_HTTP_USER_AGENT');
        $login     = new Login;
        
        $login->setIp($ip)
              ->setUserAgent($userAgent);
        
        $this->logger->log(sprintf('Acesso da identidade:%s', $identity));
        
        if (! $login->authenticate($identity, $secret)) {
            return Response::error($login->getErrorMessage());
        }
        
        $user = ModelUser::findFirstByIdentity($identity);
        
        return Response::success([
            'login' => [
                'name' => $user->name, 
                'user_id' => $user->id, 
                'token' => $login->getToken()
            ]
        ]);
    }
    
    public function forgot() 
    {
        $email = $this->request->getPost('email');
        
        $user  = ModelUser::findFirstByEmail($email);
        
        if(!$user){
            return Response::error('Email não encontrado!');
        }
        $secret = substr(str_shuffle("ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789"), 0, 10);



        $data['secret'] = password_hash($secret, CRYPT_BLOWFISH);

        if (! $user->save($data)) {
            return Response::error($user->getMessages());
            $this->flashSession->error($user->getMessages());
        }

        return Response::success(array('user' => $user->toArray(), 'secret' => $secret));
    }

    public function confirm() 
    {
        
    }
    
    public function logout() 
    {
        $userId = $this->request->getPost('userId');
        $loginToken = LoginToken::findByUserId($userId);

        if($loginToken) {
            $loginToken->delete();
        }
        return Response::success([
            'message' => 'Logout efetuado!'
        ]);        
    }
    
}