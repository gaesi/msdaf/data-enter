<?php

namespace Webservice\Authentication\Service;

use Webservice\Authentication\Model\LoginToken;
use Webservice\Service\ServiceAbstract;

class Token extends ServiceAbstract
{
    protected $ttl = 30;
    
    /**
     * @param string $token
     * @return LoginToken|null
     */
    public function get($token)
    {
        $tokenModel = LoginToken::findFirstByToken($token);
        
        if (! $tokenModel) {
            $this->errorMessage = 'Token inválido.';
            return null;
        }
        
        if (! $this->isValid($tokenModel)) {
            return null;
        }
        
        return $tokenModel;
    }
    
    /**
     * @param Webservice\Authentication\Model\LoginToken $token
     * @return boolean
     */
    public function isValid(LoginToken $token)
    {
        $now    = time();
        $expire = strtotime($token->expire) + $this->ttl;
        
        if ($expire < $now) {
            $this->errorMessage = 'Token expirado.';
            return false;
        }
        
        return true;
    }
}