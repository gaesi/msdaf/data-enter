<?php

namespace Webservice\Authentication\Service;

use Webservice\Common\Model\User;
use Webservice\Authentication\Model\Login as LoginModel;
use Webservice\Authentication\Model\LoginFail;
use Webservice\Authentication\Model\LoginToken;
use League\OAuth2\Server\Util\SecureKey;
use Webservice\Service\ServiceAbstract;
use Webservice\Common\Service\Smtp;

class Login extends ServiceAbstract
{
    protected $ip;
    protected $userAgent;
    protected $token;
    protected $userId;
    protected $expire = 30;
    
    public function getToken()
    {
        return $this->token;
    }
    
    public function setIp($ip)
    {
        $this->ip = $ip;
        return $this;
    }
    
    public function setUserAgent($userAgent)
    {
        $this->userAgent = $userAgent;
        return $this;
    }
    
    public function authenticate($identity, $secret)
    {
        $user = User::findFirstByIdentity($identity);
        
        if (! $user) {
            $this->attempt('fail', $secret);
            return false;
        }
        
        $this->userId = $user->id;
        
        if ($this->isBlocked()) {
            $this->errorMessage = 'Acesso bloqueado.';
            return false;
        }
        
        if (! password_verify($secret, $user->secret)) {
            $this->attempt('fail', $secret);
            return false;
        }
        
        $this->generateToken();
        $this->attempt('success');
        return true;
    }
    
    public function isBlocked()
    {
        $attempt = $this->getLastFailAttempt();
        
        if ($attempt >= 3) {
            return true;
        }
        
        return false;
    }
    
    public function attempt($type, $secret = null)
    {
        if ($type == 'success') {
            $login = new LoginModel;
            $login->save([
               'user_id' =>  $this->userId,
               'ip' => $this->ip,
               'user_agent' => $this->userAgent
            ]);
            $attemptReset=LoginFail::find("user_id=$this->userId");
            foreach ($attemptReset as $fail) {
                $fail->update(['attempt' => 0]);
            }
            return true;
        }
        
        $attempt = $this->getLastFailAttempt();
        
        $loginFail = new LoginFail;
        $data = [
            'user_id' =>  $this->userId,
            'ip' => $this->ip,
            'secret' => $secret,
            'user_agent' => $this->userAgent,
            'attempt' => ++$attempt,
            'created'=> new \Phalcon\Db\RawValue('default')
        ];
        
        if (! (int) $this->userId) {
            unset($data['user_id']);
        }
        
        $loginFail->save($data);
        
        if ($attempt >= 3) {
            $user = User::findFirstById($this->userId);
            $di = \Phalcon\DI\FactoryDefault::getDefault();
            $configMail = $di->get('config')['mail'];
            $userAdmin = (new User)->getAdminUser();
            $smtp = new Smtp();
            $to=array();
            foreach($userAdmin as $admin){
                $to[]=$admin['email'];
            }
            $emailBody = "Você está recebendo este e-mail porque o usuario: <b style='color: red;'>".$user->name."</b> excedeu as tentativas de acesso e foi bloqueado.<br>";
            $emailBody.= " Clique no icone da engrenagem e selecione \"Adminstracao\". No menu \"Usuarios\" encontre o usuario informado e click em desloquear.<br><br>";
            $emailBody.= " No menu \"Usuarios\" encontre o usuario informado e click em desloquear.<br><br>";
            $smtp->send($to, 'Acesso bloqueado', $emailBody, $configMail);

            $this->errorMessage = 'Tentativas excedidas. Acesso bloqueado.';
            return false;
        }
        
        $this->errorMessage = 'Usuário ou senha inválido';
        return false;
    }
    
    public function getLastFailAttempt()
    {
        $conditions = "(user_id = ?0 AND ip = ?1 AND user_agent = ?2)";
        $parameters = [$this->userId, $this->ip, $this->userAgent];
        $fails = LoginFail::find([
            $conditions,
            'bind' => $parameters,
            "order" => "attempt DESC"
        ]);
        
        if (count($fails)) {
            return $fails[0]->attempt;
        }
        
        return 0;
    }
    
    //@todo mover método para dentro do Service\Token
    public function generateToken()
    {
        $token = SecureKey::generate(); 
        
        $loginToken = new LoginToken;
        $loginToken->save([
            'user_id' =>  $this->userId,
            'ip' => $this->ip,
            'token' => $token,
            'user_agent' => $this->userAgent,
            'created' => new \Phalcon\Db\RawValue('default'),
            'expire' => date('Y-m-d H:i:sP', mktime(date("H"), date("i") + $this->expire))
        ]);
        
        $this->token = $token;
    }
}