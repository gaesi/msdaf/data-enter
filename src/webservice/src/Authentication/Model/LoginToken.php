<?php

namespace Webservice\Authentication\Model;

use Phalcon\Mvc\Model;
use Phalcon\Db\RawValue;

class LoginToken extends Model 
{
    public $id;
    public $user_id;
    public $role_id;
    public $ip;
    public $user_agent;
    public $attempt;
    public $created;

    public function initialize() 
    {
        $this->setSchema("public");
        $this->setSource("login_token");
        
        $this->belongsTo(
            'user_id', 'Webservice\Common\Model\User', 'id',
            ['foreignKey' => true, 'alias' => 'user']
        );
    }

    public function beforeValidationOnCreate()
    {
        $this->created = new RawValue('default');
    }        
}