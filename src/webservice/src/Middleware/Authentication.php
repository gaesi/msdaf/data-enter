<?php

namespace Webservice\Middleware;

use Phalcon\Mvc\Micro as Micro;
use Phalcon\Mvc\Micro\MiddlewareInterface;
use League\OAuth2\Server\Exception\InvalidRequestException;

class Authentication implements MiddlewareInterface
{
    protected $app;

    public function call(Micro $app)
    {
        //call(Micro $app)
        $this->app = $app;

        return $this->isValidRequest() &&
        $this->isValidUser() &&
        $this->isValidAcl();
    }

    protected function isValidRequest()
    {
        $this->checkRequiredHeaders();

        $uri = $this->app->request->getURI();

        if (strpos($uri, '/oauth/token') > -1) {
            return true;
        }

        $this->app->request->getPut(); //Necessário para o bug de não ter parâmetros no PUT

        return $this->app->oauth_resource->isValidRequest();
    }


    protected function checkRequiredHeaders()
    {
        $headers = apache_request_headers();
        if (! isset($headers['X_AUTH_IP'])
            || ! isset($headers['X_AUTH_HTTP_USER_AGENT']))
        {
            $message = 'As headers X_AUTH_IP e X_AUTH_HTTP_USER_AGENT devem ser informadas.';
            throw new \InvalidArgumentException($message);
        }
    }

    protected function refreshToken()
    {
        //atualizar o tempo de expiração do token
    }

    protected function isValidUser()
    {
        //implementar validação de usuario
        $this->refreshToken();
        return true;
    }

    protected function isValidAcl()
    {
        //implementar validação de acl para usuário
        return true;
    }

    private function getCredentials()
    {
        $request = $this->app->request;
        $key     = $request->hasPut('key') ? $request->getPut('key') : $request->get('key');
        $secret  = $request->hasPut('secret') ? $request->getPut('secret') : $request->get('secret');

        return compact('key', 'secret');
    }
}
