<?php

namespace Webservice\Common\Controller;

use Phalcon\Mvc\Controller;
use Webservice\Mvc\Response;
use Webservice\Common\Service\System as SystemService;

class System extends Controller
{
    public function index()
    {
        $system = (new SystemService)->getAll();

        if (! $system) {
            return Response::error('Nenhum sistema cadastrado.');
        }

        return Response::success(['system' => $system->toArray()]);
    }

    public function view($id)
    {
        $system = (new SystemService)->getById($id);

        if (! $system) {
            return Response::error('Nenhum sistema cadastrado.');
        }

        return Response::success(['system' => $system->toArray()]);
    }
}