<?php

namespace Webservice\Common\Controller;

use Phalcon\Mvc\Controller;
use Webservice\Mvc\Response;
use Webservice\Common\Service\User as UserService;
use Webservice\Acl\Service\Role as RoleService;
use Webservice\Authentication\Model\LoginToken;

class User extends Controller 
{

    public function view($userId)
    {

        $user = (new UserService)->getById($userId);
        $loginToken = LoginToken::findByUserId($userId);

        if (! $user ) {
            return Response::error('Nenhum sistema cadastrado.');
        }
        if ( count($loginToken->toArray())==0 )  {
            return Response::error('Token expirado.');
        }

        return Response::success(['user' => $user->toArray()]);

    }

    public function roles()
    {
        $systemId = $this->request->get('system_id');
        $token    = getallheaders()['X_AUTH_TOKEN']; //$this->request->getHeader('X_AUTH_TOKEN');
        
        $this->logger->log(sprintf('roles - sistema:%s, token:%s', $systemId, $token));
        
        if (! ($systemId && $token)) {
            return Response::error('O token de autenticação e o id do sistema devem ser informados.');
        }
        
        $userService = new UserService;
        $userModel   = $userService->getByToken($token);
        
        if (! $userModel) {
            return Response::error($userService->getErrorMessage());
        }
        
        $this->logger->log(sprintf('roles - user:%s', $userModel->id));
        
        $role = new RoleService;
        $roles = $role->getByUserAndSystem($userModel->id, $systemId);
        
        if (! count($roles)) {
            return Response::error('Usuário sem acesso ao sistema requisitado.');
        }
        
        return Response::success(['roles' => $roles]);
    }

    public function systems($userId)
    {
        if (! $userId) {
            return Response::error('O id do usuário deve ser informado.');
        }

        $systems = (new UserService)->getSystems($userId);

        if (! $systems) {
            return Response::error('Usuário não possui sistemas vinculados.');
        }

        return Response::success($systems);
    }

    public function lastAccess($userId)
    {
        if (! $userId) {
            return Response::error('O id do usuário deve ser informado.');
        }

        $lastAccess = [];
        $response   = (new UserService)->lastAccess($userId);

        if ($response) {
            $lastAccess = $response->toArray();
        }
        
        return Response::success($lastAccess);
    }
    
    public function validate()
    {
        $roleId   = $this->request->get('role_id');
        $systemId = $this->request->get('system_id');
        $token    = $this->request->get('token');
        
        $this->logger->log(sprintf('validate - sistema:%s, role:%s, token:%s', $systemId, $roleId, $token));
        
        if (! ($roleId && $systemId && $token)) {
            return Response::error('O token de autenticação, id do sistema e o id do perfil devem ser informados.');
        }
        
        $userService = new UserService;
        $userModel   = $userService->getByToken($token);
        
        if (! $userModel) {
            return Response::error($userService->getErrorMessage());
        }
        
        $role = new RoleService;
        $access = $role->validateAccess($userModel->id, $systemId, $roleId);
        
        return Response::success([
            'validate' => [
                'access' => $access,
                'userId' => $userModel->id,
                'userName' => $userModel->name,
                'identity' => $userModel->identity,
                'property' => $userModel->property
            ]
        ]);
    }
    
    public function isAllowed()
    {
        $headers  = getallheaders();
        $resource = $this->request->get('resource');
        $token    = $headers['X_AUTH_TOKEN']; //$this->request->getHeader('X_AUTH_TOKEN');
        $roleId   = $headers['X_AUTH_ROLE']; //$this->request->getHeader('X_AUTH_ROLE');
        $systemId = $headers['X_AUTH_SYSTEM']; //$this->request->getHeader('X_AUTH_SYSTEM');
        
        $this->logger->log(sprintf('isAllowed - sistema:%s, role:%s, recurso:%s', $systemId, $roleId, $systemId));
        
        if (! ($resource && $token)) {
            return Response::error('O token e recurso devem ser informados.');
        }
        
        $role = new RoleService;
        
        if (! $roleId) {
            $roleId = $role->getIdByName('Identificado');
        }
        
        $access = $role->validateResourceAccess($roleId, $resource);
        
        return Response::success([
            'acl' => ['access' => $access]
        ]);
    }
}