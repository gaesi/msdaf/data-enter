<?php

namespace Webservice\Common\Service;

use Webservice\Service\ServiceAbstract;
use Webservice\Common\Model\System as SystemModel;

class System extends ServiceAbstract
{
    public function getAll()
    {
        return SystemModel::find();
    }

    public function getById($id)
    {
        return SystemModel::findFirstById($id);
    }
}