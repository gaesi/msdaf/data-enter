<?php

namespace Webservice\Common\Service;

use Webservice\Service\ServiceAbstract;
use Webservice\Authentication\Service\Token;
use Webservice\Acl\Model\UserRole;
use Webservice\Common\Model\System;
use Webservice\Authentication\Model\Login;
use Webservice\Common\Model\User as UserModel;

class User extends ServiceAbstract
{

    public function getById($id)
    {
        return UserModel::findFirstById($id);
    }

    public function getByToken($token)
    {
        $tokenService = new Token;
        $tokenModel = $tokenService->get($token);

        if (! $tokenModel) {
            $this->errorMessage = $tokenService->getErrorMessage();
            return null;
        }

        return $tokenModel->user;
    }

    public function getSystems($userId)
    {

        $di = \Phalcon\DI\FactoryDefault::getDefault();
        $db = $di->get('db');
        $bind['user_id'] = $userId;

        $sql       = "SELECT distinct(s.id), s.name, replace(s.return_url, '/validate', '') as return_url, s.key FROM system s "
            ."INNER JOIN user_role as ur ON s.id = ur.system_id "
            ."WHERE ur.user_id = :user_id AND visible = true "
            ."ORDER BY s.name ASC";
        $data = $db->fetchAll($sql, \Phalcon\Db::FETCH_ASSOC, $bind);

        return $data;
    }


    public function lastAccess($userId)
    {
        $login = Login::find([
            "columns"    => "created",
            "conditions" => "user_id = ?0",
            "bind"       => [$userId],
            "order"      => "id DESC",
            "limit"      => array(1,1),
        ]);


        return $login->getFirst();
    }
}