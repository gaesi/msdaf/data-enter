<?php

namespace Webservice\Common\Model;

use Phalcon\Mvc\Model;

class System extends Model 
{
    public $id;
    public $name;
    public $key;
    public $secret;
    public $return_url;
    
    public function initialize() 
    {
        $this->setSchema("public");
        $this->setSource("system");
    }
}