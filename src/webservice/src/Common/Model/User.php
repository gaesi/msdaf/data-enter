<?php

namespace Webservice\Common\Model;

use Phalcon\Mvc\Model;
use Phalcon\Mvc\Model\Relation;

class User extends Model {

    public $id;
    public $name;
    public $email;
    public $identity;
    public $secret;
    public $property;

    public function initialize() {
        $this->setSchema("public");
        $this->setSource("user");
        
        $this->hasMany(
            'id', 'Webservice\Authentication\Model\Login', 'user_id',
            ['foreignKey' => ['action' => Relation::ACTION_CASCADE]]
        );
        $this->hasMany(
            'id', 'Webservice\Authentication\Model\LoginFail', 'user_id',
            ['foreignKey' => ['action' => Relation::ACTION_CASCADE]]
        );
        $this->hasMany(
            'id', 'Webservice\Authentication\Model\LoginReset', 'user_id',
            ['foreignKey' => ['action' => Relation::ACTION_CASCADE]]
        );
    }

    public function getAdminUser()
    {

        $di = \Phalcon\DI\FactoryDefault::getDefault();
        $db = $di->get('db');

        $sql       = "
                    SELECT  DISTINCT email FROM public.user as user_
                    inner join public.user_role user_role_ ON user_.id=user_role_.user_id
                    INNER JOIN public.role role_ ON user_role_.role_id=role_.id
                    where role_.name='Administrador'
        ";
        $data = $db->fetchAll($sql, \Phalcon\Db::FETCH_ASSOC);

        return $data;
    }


}
