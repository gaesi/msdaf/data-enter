<?php
function x( $vars )
{
    echo "<div id='x'>";
    foreach ( func_get_args() as $idx => $arg ) {
        echo "<b><u>ARG[$idx]</u></b><br><pre>";
        print_r( $arg );
        echo "</pre>";
    }
    echo "</div><br><br>";
    flush();
}
use Webservice\Mvc\Micro;
use Webservice\Middleware\Authentication;
use JSend\JSendResponse;
use Phalcon\Db\Adapter\Pdo\Postgresql;

define('APPLICATION_ENV', getenv('APPLICATION_ENV'));
define('APPLICATION_PATH', realpath('../'));

require APPLICATION_PATH . '/vendor/autoload.php';

try {
    $app = new Micro();
    $app->setConfig(APPLICATION_PATH . '/config/app.ini', APPLICATION_ENV);

    $app->before(new Authentication());
    
    $app->handle();
} catch (Exception $e) {
    $jsend = new JSendResponse('error', [], $e->getMessage(), $e->getCode());
    $app->response->setHeader("Content-Type", "application/json");    
    $app->response->setContent($jsend->encode() );
    $app->response->send();
}