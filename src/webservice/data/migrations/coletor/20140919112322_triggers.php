<?php

use Phinx\Migration\AbstractMigration;

class Triggers extends AbstractMigration
{
    /**
     * Change Method.
     *
     * More information on this method is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-change-method
     *
     * Uncomment this method if you would like to use it.
     *
    public function change()
    {
    }
    */
    
    /**
     * Migrate Up.
     */
    public function up()
    {
        $this->execute(file_get_contents(__DIR__ . '/triggers/create_table.sql'));
        $this->execute(file_get_contents(__DIR__ . '/triggers/insert_data.sql'));
    }

    /**
     * Migrate Down.
     */
    public function down()
    {

    }
}