-- Function: insert_data()

-- DROP FUNCTION insert_data();

CREATE OR REPLACE FUNCTION insert_data()
  RETURNS trigger AS
$BODY$
    DECLARE
    struct_json json;
    struct_json_text record;
    schemaname varchar;
    tablename varchar;
    tablename_type varchar;
    field_line varchar;
    elements varchar[];
    elementscomment varchar[];
    elementsconvert varchar[];
    field_line_convert varchar;    
    quotename varchar;
    name_field varchar;
    name_type varchar;
    name_lengh varchar;
    precision_int varchar;
    name_comment varchar;
    required varchar;
    null_col varchar;
    count int;
    countconvert int;
    r int;
    ref RECORD;
    ref2 RECORD;
    data_new RECORD;
    BEGIN	
	count:=1;
	countconvert:=1;
	schemaname:='data_store';
        IF (TG_OP = 'DELETE') THEN
		SELECT name INTO ref FROM public.form where id=OLD.form_id LIMIT 1;
		tablename:=unaccent_string(regexp_replace(ref.name, '\W', '', 'g'));
		tablename_type:=tablename||'_type';
		EXECUTE 'DELETE FROM '||schemaname||'.'||tablename||' WHERE id='||OLD.id||';';
        ELSIF (TG_OP = 'UPDATE') THEN
	    SELECT name,struct INTO ref FROM public.form where id=NEW.form_id LIMIT 1;
		FOR struct_json IN select * from json_array_elements(ref.struct)
		LOOP
			quotename:='"';
			name_field:=json_extract_path_text(struct_json,'nativeName');
			name_type:=json_extract_path_text(struct_json,'type');
			elementsconvert[countconvert]:='popo.'||name_field;
			IF (name_type='map'::varchar) THEN
				elementsconvert[countconvert]:='ST_MakePoint( (string_to_array( popo.'||name_field||','',''))[2]::double precision, (string_to_array( popo.'||name_field||','',''))[1]::double precision)';
			END IF;
			countconvert:=countconvert+1;
		END LOOP;
		tablename:=unaccent_string(regexp_replace(ref.name, '\W', '', 'g'));
		tablename_type:=tablename||'_type';
		field_line_convert:=array_to_string(elementsconvert, ',');
		EXECUTE 'TRUNCATE TABLE '||schemaname||'.'||tablename||';';
		EXECUTE 'INSERT INTO '||schemaname||'.'||tablename||' SELECT id,(CASE WHEN updated IS NOT NULL THEN updated ELSE created END) as date_row, '||field_line_convert||' FROM data as a, LATERAL(SELECT * FROM json_populate_recordset(null::'||tablename_type||', (SELECT json_agg(value) FROM data WHERE data.id = a.id))) AS popo WHERE form_id  = '||NEW.form_id||'; ';

        ELSIF (TG_OP = 'INSERT') THEN
		SELECT name,struct INTO ref FROM public.form where id=NEW.form_id LIMIT 1;
		FOR struct_json IN select * from json_array_elements(ref.struct)
		LOOP
			quotename:='"';
			name_field:=struct_json->'nativeName';
			name_type:=replace((struct_json->'type'::varchar)::text,quotename::text,''::text);
			elementsconvert[countconvert]:='popo.'||name_field;
			IF (name_type='map'::varchar) THEN
				elementsconvert[countconvert]:='ST_MakePoint( (string_to_array( popo.'||name_field||','',''))[2]::double precision, (string_to_array( popo.'||name_field||','',''))[1]::double precision)';
			END IF;
			countconvert:=countconvert+1;
		END LOOP;
		tablename:=unaccent_string(regexp_replace(ref.name, '\W', '', 'g'));
		tablename_type:=tablename||'_type';
		field_line_convert:=array_to_string(elementsconvert, ',');
		EXECUTE 'INSERT INTO '||schemaname||'.'||tablename||' SELECT id,(CASE WHEN updated IS NOT NULL THEN updated ELSE created END) as date_row, '||field_line_convert||' FROM data as a, LATERAL(SELECT * FROM json_populate_recordset(null::'||tablename_type||', (SELECT json_agg(value) FROM data WHERE data.id = a.id))) AS popo WHERE a.id  = '||NEW.id||'; ';
        END IF;

        RETURN NULL;
    END;
$BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100;

DROP TRIGGER IF EXISTS manager_data ON data;

CREATE TRIGGER manager_data
  AFTER INSERT OR UPDATE OR DELETE
  ON data
  FOR EACH ROW
  EXECUTE PROCEDURE insert_data();


