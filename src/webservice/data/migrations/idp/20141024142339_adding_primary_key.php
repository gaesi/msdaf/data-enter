<?php

use Phinx\Migration\AbstractMigration;

class AddingPrimaryKey extends AbstractMigration
{
    /**
     * Change Method.
     *
     * More information on this method is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-change-method
     *
     * Uncomment this method if you would like to use it.
     *
    public function change()
    {
    }
    */
    
    /**
     * Migrate Up.
     */
    public function up()
    {
        $this->table('role_resource')->drop();
        
        $roleResource = $this->table('role_resource', ['id' => false, 'primary_key' => ['role_id', 'resource_id']]);
        $roleResource->addColumn('role_id', 'integer')
                     ->addColumn('resource_id', 'integer')
                     ->addIndex(['role_id', 'resource_id'], ['unique' => true])
                     ->save();
    }

    /**
     * Migrate Down.
     */
    public function down()
    {

    }
}