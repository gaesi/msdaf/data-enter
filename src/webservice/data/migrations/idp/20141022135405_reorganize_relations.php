<?php

use Phinx\Migration\AbstractMigration;

class ReorganizeRelations extends AbstractMigration
{
    /**
     * Change Method.
     *
     * More information on this method is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-change-method
     *
     * Uncomment this method if you would like to use it.
     *
    public function change()
    {
    }
    */
    
    /**
     * Migrate Up.
     */
    public function up()
    {
        $this->execute('TRUNCATE role_resource, resource;');
        
        $roleResource = $this->table('role_resource', ['id' => false, 'primary_key' => ['role_id', 'resource_id']]);
        $roleResource->removeColumn('system_id')
                     ->addIndex(['role_id', 'resource_id'], ['unique' => true])
                     ->save();
        
        $resource = $this->table('resource');
        $resource->addColumn('system_id', 'integer')
                 ->addIndex(['system_id'])
                 ->addForeignKey('system_id', '"system"')
                 ->save();
    }

    /**
     * Migrate Down.
     */
    public function down()
    {

    }
}