<?php

use Phinx\Migration\AbstractMigration;

class CargaInicial extends AbstractMigration
{
    
    /**
     * Migrate Up.
     */
    public function up()
    {
        $this->execute("INSERT INTO public.user(name, identity, secret, email) VALUES ('Administrador do Sistema', 'admin', '$2y$10\$nO0VfVaQZhC.ofemsila5./bLCIdUY1EBdce2xLIPaiYckCHQGnLK', 'abdala.cerqueira@gmail.com')");

    }

    /**
     * Migrate Down.
     */
    public function down()
    {

    }
}