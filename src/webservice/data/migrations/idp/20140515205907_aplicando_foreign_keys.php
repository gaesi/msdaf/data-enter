<?php

use Phinx\Migration\AbstractMigration;

class AplicandoForeignKeys extends AbstractMigration {

    /**
     * Migrate Up.
     */
    public function up() {

        $userRole = $this->table('user_role');
        $userRole->addForeignKey('role_id', '"role"');
        $userRole->addForeignKey('system_id', '"system"');
        $userRole->addForeignKey('user_id', '"user"');
        $userRole->save();

        $roleResource = $this->table('role_resource');
        $roleResource->addForeignKey('role_id', '"role"');
        $roleResource->addForeignKey('resource_id', '"resource"');
        $roleResource->addForeignKey('system_id', '"system"');
        $roleResource->save();

        $login = $this->table('login');
        $login->addForeignKey('user_id', '"user"');
        $login->save();

        $loginFail = $this->table('login_fail');
        $loginFail->addForeignKey('user_id', '"user"');
        $loginFail->save();

        $loginReset = $this->table('login_reset');
        $loginReset->addForeignKey('user_id', '"user"');
        $loginReset->save();

        $loginToken = $this->table('login_token');
        $loginToken->removeColumn('role_id');
        $loginToken->addForeignKey('user_id', '"user"');
        $loginToken->save();

        $registration = $this->table('registration');
        $registration->addForeignKey('user_id', '"user"');
        $registration->save();
    }

    /**
     * Migrate Down.
     */
    public function down() {
        $userRole = $this->table('user_role');
        $userRole->dropForeignKey('role_id');
        $userRole->dropForeignKey('system_id');
        $userRole->dropForeignKey('user_id');
        $userRole->save();

        $roleResource = $this->table('role_resource');
        $roleResource->dropForeignKey('role_id');
        $roleResource->dropForeignKey('resource_id');
        $roleResource->dropForeignKey('system_id');
        $roleResource->save();
        
        $login = $this->table('login');
        $login->dropForeignKey('user_id');
        $login->save();

        $loginFail = $this->table('login_fail');
        $loginFail->dropForeignKey('user_id');
        $loginFail->save();
        
        $loginToken = $this->table('login_token');
        $loginToken->addColumn('role_id', 'integer', array('after' => 'id'));
        $loginToken->dropForeignKey('user_id');
        $loginToken->save();
        
        $registration = $this->table('registration');
        $registration->dropForeignKey('user_id');
        $registration->save();
    }

}
