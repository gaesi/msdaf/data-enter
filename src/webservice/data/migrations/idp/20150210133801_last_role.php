<?php

use Phinx\Migration\AbstractMigration;

class LastRole extends AbstractMigration
{
    /**
     * Change Method.
     *
     * More information on this method is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-change-method
     *
     * Uncomment this method if you would like to use it.
     *
    public function change()
    {
    }
    */
    
    /**
     * Migrate Up.
     */
    public function up()
    {
        $lastRole = $this->table('user_last_role');
        $lastRole->addColumn('role_id', 'integer')
                 ->addColumn('user_id', 'integer')
                 ->addColumn('system_id', 'integer')
                 ->addColumn('created', 'timestamp', array('default' => 'CURRENT_TIMESTAMP'))
                 ->addIndex(['role_id'])
                 ->addIndex(['user_id'])
                 ->addIndex(['system_id'])
                 ->addForeignKey('role_id', '"role"')
                 ->addForeignKey('user_id', '"user"')
                 ->addForeignKey('system_id', '"system"')
                 ->save();
    }

    /**
     * Migrate Down.
     */
    public function down()
    {

    }
}